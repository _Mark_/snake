﻿using GameMechanics.TransferTrigger;
using UnityEditor;

namespace Editor.TransferTrigger
{
  [CustomEditor(typeof(LevelTransferTriggerMarker))]
  public class LevelTransferTriggerMarkerEditor : UnityEditor.Editor
  {
    [DrawGizmo(GizmoType.Selected | GizmoType.NonSelected | GizmoType.Pickable)]
    public static void DrawColliderLevelTransferTrigger(LevelTransferTriggerMarker TransferTriggerMarker, GizmoType gizmoType) =>
      GizmosLevelTransferTrigger.DrawColliderLevelTransferTrigger(TransferTriggerMarker);
  }
}