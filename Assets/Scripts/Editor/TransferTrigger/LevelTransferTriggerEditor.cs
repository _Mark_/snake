﻿using GameMechanics.TransferTrigger;
using UnityEditor;

namespace Editor.TransferTrigger
{
  [CustomEditor(typeof(LevelTransferTrigger))]
  public class LevelTransferTriggerEditor : UnityEditor.Editor
  {
    [DrawGizmo(GizmoType.Selected | GizmoType.NonSelected | GizmoType.Pickable)]
    public static void DrawColliderLevelTransferTrigger(LevelTransferTrigger TransferTrigger, GizmoType gizmoType) =>
      GizmosLevelTransferTrigger.DrawColliderLevelTransferTrigger(TransferTrigger);
  }
}