﻿using UnityEngine;

namespace Editor.TransferTrigger
{
  public static class GizmosLevelTransferTrigger
  {
    private static readonly Color32 _boxColor = new Color32(120, 120, 190, 150);
    
    public static void DrawColliderLevelTransferTrigger(Component component)
    {
      BoxCollider boxCollider = component.GetComponent<BoxCollider>();
      Gizmos.color = _boxColor;
      Gizmos.DrawCube(boxCollider.transform.position + boxCollider.center, boxCollider.size);
    }
  }
}