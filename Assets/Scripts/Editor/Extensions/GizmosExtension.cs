﻿using UnityEngine;

namespace Editor.Extensions
{
  public static class GizmosExtension
  {
    public static void DrawCustomBox(Bounds bounds)
    {
      float x = bounds.max.x;
      float y = bounds.max.y;
      float z = bounds.max.z;
      float _x = bounds.min.x;
      float _y = bounds.min.y;
      float _z = bounds.min.z;

      Vector3 upperForwardLeft = new Vector3(_x, y, z);
      Vector3 upperForwardRight = new Vector3(x, y, z);
      Vector3 upperBackLeft = new Vector3(_x, y, _z);
      Vector3 upperBackRight = new Vector3(x, y, _z);

      Vector3 downForwardLeft = new Vector3(_x, _y, z);
      Vector3 downForwardRight = new Vector3(x, _y, z);
      Vector3 downBackLeft = new Vector3(_x, _y, _z);
      Vector3 downBackRight = new Vector3(x, _y, _z);

      // Down and upper squares.
      DrawSquare(upperForwardLeft, upperForwardRight, upperBackRight, upperBackLeft);
      DrawSquare(downForwardLeft, downForwardRight, downBackRight, downBackLeft);

      // Vertical edges. 
      Gizmos.DrawLine(upperForwardLeft, downForwardLeft);
      Gizmos.DrawLine(upperForwardRight, downForwardRight);
      Gizmos.DrawLine(upperBackLeft, downBackLeft);
      Gizmos.DrawLine(upperBackRight, downBackRight);
    }

    public static void DrawSquare(Vector3 upperLeft, Vector3 upperRight, Vector3 downRight, Vector3 downLeft)
    {
      Gizmos.DrawLine(upperLeft, upperRight);
      Gizmos.DrawLine(upperRight, downRight);
      Gizmos.DrawLine(downRight, downLeft);
      Gizmos.DrawLine(downLeft, upperLeft);
    }
  }
}