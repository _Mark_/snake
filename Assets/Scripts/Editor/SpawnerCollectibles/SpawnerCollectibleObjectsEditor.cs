﻿using GameMechanics.SpawnerCollectibles;
using UnityEditor;
using UnityEngine;

namespace Editor.SpawnerCollectibles
{
  [CustomEditor(typeof(SpawnerCollectibleObjects))]
  public class SpawnerCollectibleObjectsEditor : UnityEditor.Editor
  {
    private const float _radiusPoints = .5f;

    private SpawnerCollectibleObjects _spawnerCollectibleObjects;

    private void OnEnable() =>
      _spawnerCollectibleObjects = (SpawnerCollectibleObjects) target;

    public void OnSceneGUI()
    {
      DrawPointsPositionHandle();
      ResetSpawnerRotation();
    }

    private void DrawPointsPositionHandle()
    {
      EditorGUI.BeginChangeCheck();
      Handles.matrix = _spawnerCollectibleObjects.transform.localToWorldMatrix;
      Vector3 firstPoint = Handles.PositionHandle(_spawnerCollectibleObjects.FirstPoint, Quaternion.identity);
      Vector3 secondPoint = Handles.PositionHandle(_spawnerCollectibleObjects.SecondPoint, Quaternion.identity);
      if (EditorGUI.EndChangeCheck())
      {
        Undo.RecordObject(_spawnerCollectibleObjects, $"Changed \"_spawnerCollectibleObjects\".");
        _spawnerCollectibleObjects.FirstPoint = firstPoint;
        _spawnerCollectibleObjects.SecondPoint = secondPoint;
      }
    }

    [DrawGizmo(GizmoType.Selected | GizmoType.NonSelected)]
    private static void DrawPoints(SpawnerCollectibleObjects spawnerCollectibleObjects, GizmoType gizmoType)
    {
      Gizmos.matrix = spawnerCollectibleObjects.transform.localToWorldMatrix;
      DrawSphereOnPoints(spawnerCollectibleObjects);
      Gizmos.color = Color.white;
      Gizmos.DrawWireCube(Center(spawnerCollectibleObjects), Size(spawnerCollectibleObjects));
    }

    private static Vector3 Size(SpawnerCollectibleObjects spawnerCollectibleObjects)
    {
      float x = Mathf.Abs(spawnerCollectibleObjects.FirstPoint.x - spawnerCollectibleObjects.SecondPoint.x);
      float y = Mathf.Abs(spawnerCollectibleObjects.FirstPoint.y - spawnerCollectibleObjects.SecondPoint.y);
      float z = Mathf.Abs(spawnerCollectibleObjects.FirstPoint.z - spawnerCollectibleObjects.SecondPoint.z);
      return new Vector3(x, y, z);
    }

    private static Vector3 Center(SpawnerCollectibleObjects spawnerCollectibleObjects)
    {
      float x = spawnerCollectibleObjects.FirstPoint.x + spawnerCollectibleObjects.SecondPoint.x;
      float y = spawnerCollectibleObjects.FirstPoint.y + spawnerCollectibleObjects.SecondPoint.y;
      float z = spawnerCollectibleObjects.FirstPoint.z + spawnerCollectibleObjects.SecondPoint.z;
      return new Vector3(x / 2, y / 2, z / 2);
    }

    private static void DrawSphereOnPoints(SpawnerCollectibleObjects spawnerCollectibleObjects)
    {
      Gizmos.color = Color.red;
      Gizmos.DrawSphere(spawnerCollectibleObjects.FirstPoint, _radiusPoints);
      Gizmos.color = Color.blue;
      Gizmos.DrawSphere(spawnerCollectibleObjects.SecondPoint, _radiusPoints);
    }

    private void ResetSpawnerRotation() =>
      _spawnerCollectibleObjects.transform.rotation = Quaternion.identity;
  }
}