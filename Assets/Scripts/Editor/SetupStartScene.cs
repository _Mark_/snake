﻿using UnityEditor;
using UnityEngine;

namespace Editor
{
  public static class SetupStartScene
  {
    [MenuItem("GameObject/Setups/Create start GameObjects", false, 0)]
    private static void SetStartObjectsScene()
    {
      var servicesParant = new GameObject("[SERVICES]");
      var UIParant = new GameObject("[UI]");
      var lightParant = new GameObject("[LIGHTS]");
      var worldParant = new GameObject("[WORLD]");

      var dinamicParant = new GameObject("-DYNAMIC-");
      var staticParant = new GameObject("-STATIC-");

      staticParant.transform.parent = worldParant.transform;
      dinamicParant.transform.parent = worldParant.transform;

      Debug.Log("Созданы стартовые объекты сцены.");
    }
  }
}