﻿using System.Linq;
using StaticData;
using UnityEditor;
using UnityEngine;

namespace Editor.StaticData
{
  [CustomEditor(typeof(LevelList))]
  public class LevelListEditor : UnityEditor.Editor
  {
    private LevelList _levelList;

    private void OnEnable() =>
      _levelList = (LevelList) target;

    public override void OnInspectorGUI()
    {
      base.OnInspectorGUI();
      ButtonCollectLevelList();
    }

    private void ButtonCollectLevelList()
    {
      if (GUILayout.Button("Collect level list from \"Scenes in build\"."))
      {
        CollectScenesInBuild(_levelList); // TODO: Adds ability select non game scenes.

        Debug.Log("Level list is collected.");
        EditorUtility.SetDirty(target);
      }
    }

    public static void CollectScenesInBuild(LevelList levelList)
    {
      levelList.PathsScenes = EditorBuildSettingsScene.GetActiveSceneList(EditorBuildSettings.scenes).ToList();
      RemoveInitialLevelFromList(levelList);
      RemoveClassicModeLevelFromList(levelList);
    }

    private static void RemoveClassicModeLevelFromList(LevelList levelList) =>
      RemoveFirstLevelFromList(levelList);

    private static void RemoveInitialLevelFromList(LevelList levelList) =>
      RemoveFirstLevelFromList(levelList);

    private static void RemoveFirstLevelFromList(LevelList levelList) =>
      levelList.PathsScenes.RemoveAt(0);
  }
}