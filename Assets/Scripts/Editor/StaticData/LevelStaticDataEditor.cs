﻿using System.Linq;
using Data;
using Extensions;
using GameMechanics.Collectibles.Markers;
using GameMechanics.EnemySnake;
using GameMechanics.SpawnerCollectibles;
using GameMechanics.TransferTrigger;
using StaticData;
using StaticData.CollectibleObjects;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Editor.StaticData
{
  [CustomEditor(typeof(LevelStaticData))]
  public class LevelStaticDataEditor : UnityEditor.Editor
  {
    private LevelStaticData _levelData;

    private void OnEnable() =>
      _levelData = (LevelStaticData) target;

    public override void OnInspectorGUI()
    {
      base.OnInspectorGUI();
      ButtonCollectDataset();
    }

    private void ButtonCollectDataset()
    {
      if (GUILayout.Button("Collect dataset."))
      {
        SetLevelKey(_levelData);
        SetSpawnEnemySnakeDataset(_levelData);
        SetLevelTransferTriggerDataset(_levelData);
        SetSpawnerCollectibleObjects(_levelData);
        SetCollectables(_levelData);

        Debug.Log("Dataset level is collected.");
        EditorUtility.SetDirty(target);
      }
    }

    public static void SetLevelKey(LevelStaticData levelStaticData) =>
      levelStaticData.LevelKey = SceneManager.GetActiveScene().name;

    public static void SetSpawnEnemySnakeDataset(LevelStaticData levelStaticData) =>
      levelStaticData.SpawnEnemySnakeDataset = FindObjectsOfType<SpawnEnemySnakeMarker>()
        .Select(
          marker => new SpawnEnemySnakeDataset(
            marker.LenghtTile,
            marker.SpeedSnake,
            marker.Controller.CurrentTarget,
            marker.Controller.DistanceToChangedTargetPoint,
            marker.Controller.Points.Select(point => new Vector3Data(point)).ToList()
          )
        ).ToList();

    public static void SetLevelTransferTriggerDataset(LevelStaticData levelStaticData)
    {
      LevelTransferTriggerMarker levelTransferTriggerMarker = FindObjectOfType<LevelTransferTriggerMarker>();
      if (!levelTransferTriggerMarker) return;

      BoxCollider boxCollider = levelTransferTriggerMarker.GetComponent<BoxCollider>();
      if (boxCollider != null)
      {
        levelStaticData.LevelTransferTriggerDataset = new LevelTransferTriggerDataset(
          boxCollider.transform.position.AsVector3Data(),
          boxCollider.transform.rotation.eulerAngles.AsVector3Data(),
          boxCollider.center.AsVector3Data(),
          boxCollider.size.AsVector3Data());
      }
    }

    public static void SetSpawnerCollectibleObjects(LevelStaticData levelStaticData)
    {
      levelStaticData.SpawnerCollectibleObjects = FindObjectsOfType<SpawnerCollectibleObjectsMarker>()
        .Select(x => new SpawnerCollectibleObjectsDataset()
          {
            Spawner = new Spawner()
            {
              FirstPoint = x.FirstPoint.AsVector3Data(),
              SecondPoint = x.SecondPoint.AsVector3Data(),
              Spawns = x.Spawns
            },
            Controller = new Controller()
            {
              StartQuantityObjects = x.StartQuantityObjects,
              MaxQuantityObjects = x.MaxQuantityObjects,
              CurrentQuantityObjects = x.CurrentQuantityObjects,
              SpawnTime = x.SpawnTime
            }
          }
        ).ToList();
    }

    public static void SetCollectables(LevelStaticData levelStaticData)
    {
      levelStaticData.ApplePositions = FindObjectsOfType<AppleMarker>()
        .Select(x => new PositionData(x.transform.position))
        .ToList();

      SetStarsProgress(levelStaticData); // TODO: Excess responsibility.

      levelStaticData.BadApplePositions = FindObjectsOfType<BadAppleMarker>()
        .Select(x => new PositionData(x.transform.position))
        .ToList();

      levelStaticData.ScopePositions = FindObjectsOfType<ScopeMarker>()
        .Select(x => new PositionData(x.transform.position))
        .ToList();

      levelStaticData.SkullPositions = FindObjectsOfType<SkullMarker>()
        .Select(x => new PositionData(x.transform.position))
        .ToList();
    }

    private static void SetStarsProgress(LevelStaticData levelStaticData) =>
      levelStaticData.StarsProgress.SnakeLenghtOnLevel = levelStaticData.ApplePositions.Count;
  }
}