﻿using GameMechanics.Snake;
using UnityEditor;
using UnityEngine;

namespace Editor.Snake
{
  [CustomEditor(typeof(GravitationDirection))]
  public class GravitationDirectionEditor : UnityEditor.Editor
  {
    [DrawGizmo(GizmoType.NonSelected | GizmoType.Active)]
    private static void DrawBoxColliderWrite(GravitationDirection gravitation, GizmoType gizmoType)
    {
      Bounds bounds = Bounds(gravitation);
      Gizmos.DrawWireCube(bounds.center, bounds.size);
    }

    private static Bounds Bounds(GravitationDirection gravitation) =>
      gravitation.GetComponent<Collider>().bounds;
  }
}