﻿using GameMechanics.Snake;
using UnityEditor;
using UnityEngine;

namespace Editor.Cheats
{
  public static class TailChainsFactory
  {
    [MenuItem("CONTEXT/TailChainsFactory/Cheats/Add tail")]
    private static void AddTail(MenuCommand menuCommand)
    {
      SpawnerTailChains spawner = menuCommand.context as SpawnerTailChains;
      if (spawner)
        spawner.SpawnChain();
    }

    [MenuItem("Cheats/TailChainsFactory/Add tail &a")]
    private static void AddTailTool()
    {
      foreach (SpawnerTailChains factory in AllTailChainsFactory())
        factory.SpawnChain();

      SpawnerTailChains[] AllTailChainsFactory() =>
        Object.FindObjectsOfType<SpawnerTailChains>();
    }

    [MenuItem("Cheats/TailChainsFactory/Add 10 tail &#t")]
    private static void AddTenTailTool()
    {
      foreach (SpawnerTailChains factory in AllTailChainsFactory())
        for (int i = 0; i < 10; i++)
          factory.SpawnChain();

      SpawnerTailChains[] AllTailChainsFactory() =>
        Object.FindObjectsOfType<SpawnerTailChains>();
    }
  }
}