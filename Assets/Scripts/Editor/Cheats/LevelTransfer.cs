using Services;
using Services.Level;
using UnityEditor;
using UnityEngine.SceneManagement;

namespace Editor.Cheats
{
  public static class LevelTransfer
  {
    [MenuItem("Cheats/Level transfer/End this level %&a")]
    private static void EndCurrentLevel()
    {
      ILevelService levelService = AllServices.Container.Single<ILevelService>();
      levelService.EndLevel(SceneManager.GetActiveScene().path);
    }
  }
}