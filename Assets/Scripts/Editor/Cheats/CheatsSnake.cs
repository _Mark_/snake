using GameMechanics.Snake;
using UnityEditor;
using UnityEngine;

namespace Editor.Cheats
{
  public static class CheatsSnake
  {
    [MenuItem("Cheats/Snake/Death &d")]
    private static void KillSnake() =>
      Object.FindObjectOfType<SnakeDeath>().KillSnake();
  }
}