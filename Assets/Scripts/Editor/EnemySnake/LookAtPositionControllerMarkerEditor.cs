﻿using GameMechanics.EnemySnake;
using UnityEditor;
using UnityEngine;

namespace Editor.EnemySnake
{
  [CustomEditor(typeof(LookAtPositionControllerMarker))]
  public class MoveTargetControllerMarkerEditor : UnityEditor.Editor
  {
    private const float _radiusWireSpheres = .2f;
    private const float _screenSpaceSize = 10f;

    private LookAtPositionControllerMarker _marker;

    private void OnEnable() =>
      _marker = (LookAtPositionControllerMarker) target;

    private void OnSceneGUI() =>
      DrawPointersHandlesPosition();

    [DrawGizmo(GizmoType.NonSelected)]
    private static void DrawNonSelectedLines(LookAtPositionControllerMarker controller, GizmoType gizmoType)
    {
      if (controller.Points.Count >= 2)
      {
        Handles.color = Color.yellow;

        for (int i = 1; i < controller.Points.Count; i++)
          Handles.DrawDottedLine(controller.Points[i - 1], controller.Points[i], _screenSpaceSize);

        Handles.DrawDottedLine(controller.Points[controller.Points.Count - 1], controller.Points[0], _screenSpaceSize);
      }
    }

    [DrawGizmo(GizmoType.Selected)]
    private static void DrawSelectedLines(LookAtPositionControllerMarker controller, GizmoType gizmoType)
    {
      if (controller.Points.Count >= 2)
      {
        Handles.color = Color.blue;
        for (int i = 1; i < controller.Points.Count; i++)
          Handles.DrawLine(controller.Points[i - 1], controller.Points[i]);

        Handles.DrawLine(controller.Points[controller.Points.Count - 1], controller.Points[0]);
      }
    }

    [DrawGizmo(GizmoType.NonSelected)]
    private static void DrawSphereOnPointers(LookAtPositionControllerMarker controller, GizmoType gizmoType)
    {
      Gizmos.color = Color.blue;
      foreach (Vector3 point in controller.Points)
        Gizmos.DrawWireSphere(point, _radiusWireSpheres);
    }

    private void DrawPointersHandlesPosition()
    {
      for (var i = 0; i < _marker.Points.Count; i++)
      {
        DrawHandlesPosition(i);
        DrawLabelPoint(i, GUIStyleHandleText());
      }
    }

    private void DrawLabelPoint(int i, GUIStyle guiStyle) =>
      Handles.Label(_marker.Points[i], $"point {i}", guiStyle);

    private void DrawHandlesPosition(int i)
    {
      EditorGUI.BeginChangeCheck();
      Vector3 newPointPosition = Handles.PositionHandle(_marker.Points[i], Quaternion.identity);
      if (EditorGUI.EndChangeCheck())
      {
        Undo.RecordObject(_marker, $"Changed \"_moveTargetController.Points[i]\".");
        _marker.Points[i] = newPointPosition;
      }
    }

    private static GUIStyle GUIStyleHandleText() =>
      new GUIStyle() {normal = {textColor = Color.red}};
  }
}