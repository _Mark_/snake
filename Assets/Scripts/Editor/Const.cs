namespace Editor
{
  public static class Const
  {
    public static class AssetPath
    {
      public const string GameGunner = "Assets/Prefabs/Infrastructure/GameRunner.prefab";
      public const string AppleMarker = "Assets/Prefabs/AppleMarker.prefab";
      public const string BadAppleMarker = "Assets/Prefabs/BabAppleMarker.prefab";
      public const string ScopeMarker = "Assets/Prefabs/ScopeMarker.prefab";
      public const string ScullMarker = "Assets/Prefabs/SkullMarker.prefab";
      public const string InitialPoint = "Assets/Prefabs/InitialPoint.prefab";
      public const string TransferToNewLevelTriggerMarker = "Assets/Prefabs/LevelTransferTriggerMarker.prefab";
      public const string DeadlyWall = "Assets/Prefabs/DeadlyWall.prefab";
      public const string DoorAndPadlock = "Assets/Prefabs/DoorAndPadlock.prefab";
      public const string Key = "Assets/Prefabs/Key.prefab";
      public const string EnemySnakeTrajectoryMarker = "Assets/Prefabs/LookAtPositionControllerMarker.prefab";

      public static class StaticData
      {
        public const string Path = "Assets/Resources/StaticData";
        public const string Level = "Assets/Resources/StaticData/Level";
      }

      public static class ButtonTexture
      {
        public const string Apple = "Assets/DevelopResources/Textures/Apple.png";
        public const string BadApple = "Assets/DevelopResources/Textures/BadApple.png";
        public const string Scope = "Assets/DevelopResources/Textures/Scope.png";
        public const string Scull = "Assets/DevelopResources/Textures/Scull.png";
        public const string DeadlyWall = "Assets/DevelopResources/Textures/DeadlyWall.png";
        public const string Door = "Assets/DevelopResources/Textures/Door.png";
        public const string EnemySnake = "Assets/DevelopResources/Textures/EnemySnake.png";
      }
    }

    public static class External
    {
      public const string Asset = ".asset";
    }

    public static class NameFor
    {
      public static class StaticData
      {
        public const string LevelList = "LevelList";
      }
    }
  }
}