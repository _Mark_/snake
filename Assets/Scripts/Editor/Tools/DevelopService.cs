namespace Editor.Tools
{
  public static class DevelopService
  {
    public static readonly GameRunner GameRunner = new GameRunner();
    public static readonly LevelStaticDataCollector LevelStaticData = new LevelStaticDataCollector();
    public static readonly LevelListCollector LevelListCollector = new LevelListCollector();
    public static readonly PrefabLinker Prefab = new PrefabLinker();
  }
}