using UnityEditor;
using UnityEngine;

namespace Editor.Tools
{
  public class DevelopCreator
  {
    private const int _removingFromCamera = 10;

    public void AppleMarker()
    {
      GameObject appleMarker = InstantiatePrefab(Const.AssetPath.AppleMarker);
      appleMarker.transform.position = SpawnPosition();
    }

    public void BadAppleMarker()
    {
      GameObject badAppleMarker = InstantiatePrefab(Const.AssetPath.BadAppleMarker);
      badAppleMarker.transform.position = SpawnPosition();
    }

    public void ScopeMarker()
    {
      GameObject badAppleMarker = InstantiatePrefab(Const.AssetPath.ScopeMarker);
      badAppleMarker.transform.position = SpawnPosition();
    }

    public void ScullMarker()
    {
      GameObject badAppleMarker = InstantiatePrefab(Const.AssetPath.ScullMarker);
      badAppleMarker.transform.position = SpawnPosition();
    }

    public void InitialPoint()
    {
      GameObject initialPoint = InstantiatePrefab(Const.AssetPath.InitialPoint);
      initialPoint.transform.position = SpawnPosition();
    }

    public void TransferToNewLevelTriggerMarker()
    {
      GameObject initialPoint = InstantiatePrefab(Const.AssetPath.TransferToNewLevelTriggerMarker);
      initialPoint.transform.position = SpawnPosition();
    }

    public void DeadlyWall()
    {
      GameObject deathWall = InstantiatePrefab(Const.AssetPath.DeadlyWall);
      deathWall.transform.position = SpawnPosition();
    }

    public void DoorAndKey()
    {
      GameObject door = InstantiatePrefab(Const.AssetPath.DoorAndPadlock);
      GameObject key = InstantiatePrefab(Const.AssetPath.Key);
      door.transform.position = SpawnPosition();
      key.transform.position = SpawnPosition() - SceneView.lastActiveSceneView.camera.transform.forward * 5;
    }

    public void EnemySnakeTrajectoryMarker()
    {
      GameObject trajectory = InstantiatePrefab(Const.AssetPath.EnemySnakeTrajectoryMarker);
      trajectory.transform.position = SpawnPosition();
    }

    private static GameObject InstantiatePrefab(string assetPath)
    {
      GameObject prefab = AssetDatabase.LoadAssetAtPath<GameObject>(assetPath);
      GameObject instance = PrefabUtility.InstantiatePrefab(prefab) as GameObject;
      return instance;
    }

    private Vector3 SpawnPosition()
    {
      Transform camera = SceneView.lastActiveSceneView.camera.transform;
      return camera.position + camera.forward * _removingFromCamera;
    }
  }
}