using System.IO;
using System.Linq;
using Editor.StaticData;
using StaticData;
using UnityEditor;
using UnityEngine;

namespace Editor.Tools
{
  public class LevelListCollector
  {
    public void Collect()
    {
      LevelList levelList = LevelList();

      LevelListEditor.CollectScenesInBuild(levelList);

      Debug.Log("Level list is collected.");
      EditorUtility.SetDirty(levelList);
    }

    private static LevelList LevelList()
    {
      LevelList levelList = ExistingLevelList();
      if (!levelList)
        levelList = CreateNewLevelList();
      return levelList;
    }

    private static LevelList ExistingLevelList() =>
      AssetDatabase
        .FindAssets("t:LevelList", new[] {Const.AssetPath.StaticData.Path})
        .Select(AssetDatabase.GUIDToAssetPath)
        .Select(AssetDatabase.LoadAssetAtPath<LevelList>)
        .FirstOrDefault();

    private static LevelList CreateNewLevelList()
    {
      LevelList newLevelList = ScriptableObject.CreateInstance<LevelList>();
      newLevelList.name = Const.NameFor.StaticData.LevelList;
      AssetDatabase.CreateAsset(newLevelList, PathToNewStaticDataFile());
      return newLevelList;
    }
    
    private static string PathToNewStaticDataFile() =>
      Path.Combine(Const.AssetPath.StaticData.Path, Const.NameFor.StaticData.LevelList + Const.External.Asset);
  }
}