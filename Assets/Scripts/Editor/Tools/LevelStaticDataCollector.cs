using System.IO;
using System.Linq;
using Editor.StaticData;
using StaticData;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Editor.Tools
{
  public class LevelStaticDataCollector
  {
    public void Collect()
    {
      string activeSceneName = SceneManager.GetActiveScene().name;

      LevelStaticData staticData = LevelStaticData(activeSceneName);

      LevelStaticDataEditor.SetLevelKey(staticData);
      LevelStaticDataEditor.SetSpawnEnemySnakeDataset(staticData);
      LevelStaticDataEditor.SetLevelTransferTriggerDataset(staticData);
      LevelStaticDataEditor.SetSpawnerCollectibleObjects(staticData);
      LevelStaticDataEditor.SetCollectables(staticData);

      Debug.Log("Dataset level is collected.");
      EditorUtility.SetDirty(staticData);
    }

    public static LevelStaticData LevelStaticData(string activeSceneName)
    {
      LevelStaticData staticData = StaticDataOfThisLevel(activeSceneName);
      if (!staticData)
        staticData = CreateNewLevelStaticData();
      return staticData;
    }

    private static LevelStaticData StaticDataOfThisLevel(string activeSceneName) =>
      AssetDatabase
        .FindAssets("t:LevelStaticData", new[] {Const.AssetPath.StaticData.Level})
        .Select(AssetDatabase.GUIDToAssetPath)
        .Select(AssetDatabase.LoadAssetAtPath<LevelStaticData>)
        .FirstOrDefault(data => data.name == SceneManager.GetActiveScene().name);

    private static LevelStaticData CreateNewLevelStaticData()
    {
      LevelStaticData newData = ScriptableObject.CreateInstance<LevelStaticData>();
      newData.name = SceneManager.GetActiveScene().name;
      AssetDatabase.CreateAsset(newData, PathToNewStaticDataFile());
      return newData;
    }

    private static string PathToNewStaticDataFile() =>
      Path.Combine(Const.AssetPath.StaticData.Level, SceneManager.GetActiveScene().name + Const.External.Asset);
  }
}