using UnityEditor;

namespace Editor.Tools
{
  public class DeveloperConsoleWindow : EditorWindow
  {
    private const string _developerConsole = "Developer console";
    
    private static DevelopServiceGUI _developServiceGUI;

    [MenuItem("Tools/DeveloperConsole #&d")]
    private static void OpenThisWindow()
    {
      DeveloperConsoleWindow window = GetWindow<DeveloperConsoleWindow>(false, _developerConsole);
      _developServiceGUI = new DevelopServiceGUI();
    }

    private void OnGUI()
    {
      _developServiceGUI.OnGUI();
    }
  }
}