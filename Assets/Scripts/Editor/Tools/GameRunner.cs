using UnityEditor;
using UnityEngine;

namespace Editor.Tools
{
  public class GameRunner
  {
    public void Create()
    {
      if (GameRunnerIsExist())
      {
        Debug.Log($"\"GameRunner\" is exist!");
        return;
      }

      GameObject gameRunnerPrefab = AssetDatabase.LoadAssetAtPath<GameObject>(Const.AssetPath.GameGunner);
      GameObject gameRunner = PrefabUtility.InstantiatePrefab(gameRunnerPrefab) as GameObject;
      gameRunner.transform.position = Vector3.zero;

      Debug.Log($"\"GameRunner\" is created.");
    }

    private static Infrastructure.GameRunner GameRunnerIsExist() =>
      Object.FindObjectOfType<Infrastructure.GameRunner>();
  }
}