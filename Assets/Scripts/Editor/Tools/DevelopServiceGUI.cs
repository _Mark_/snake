using StaticData;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Editor.Tools
{
  public class DevelopServiceGUI
  {
    private const int _createButtonSize = 80;

    public void OnGUI()
    {
      DrawButtonCreateGameRunner();
      DrawButtonCollectStaticDataThisLevel();
      DrawButtonCollectLevelList();

      GUILayout.BeginVertical("box");

      DrawStaticData();
      DrawPrefabs();

      GUILayout.EndVertical();
    }

    private static void DrawStaticData()
    {
      GUILayout.Label($"Static data of \"{SceneManager.GetActiveScene().name}\" scene:");
      GUILayout.Space(5);

      TimeOfLevelFloatField();

      GUILayout.Space(15);
    }

    private static void TimeOfLevelFloatField()
    {
      LevelStaticData levelStaticData = LevelStaticData();
      levelStaticData.TimeOfLevel.Value =
        EditorGUILayout.FloatField("Time of level: ", levelStaticData.TimeOfLevel.Value);
    }

    private void DrawPrefabs()
    {
      GUILayout.Label("Prefabs:");
      GUILayout.Space(5);

      DrawButtonInitialPoint();
      DrawButtonTransferToNewLevelTrigger();

      GUILayout.BeginHorizontal();
      DrawButtonApple();
      DrawButtonBadApple();
      DrawButtonScope();
      GUILayout.EndHorizontal();

      GUILayout.BeginHorizontal();
      DrawButtonScull();
      DrawButtonDeadlyWall();
      DrawButtonDoorAndKey();
      GUILayout.EndHorizontal();

      GUILayout.BeginHorizontal();
      DrawButtonDoorEnemySnakeTrajectory();
      GUILayout.EndHorizontal();
    }

    private void DrawButtonCreateGameRunner()
    {
      if (GUILayout.Button("Create \"GameRunner\"."))
      {
        DevelopService.GameRunner.Create();
        EditorSceneManager.MarkSceneDirty(SceneManager.GetActiveScene());
      }
    }

    private void DrawButtonCollectStaticDataThisLevel()
    {
      if (GUILayout.Button("Collect static data of this level."))
        DevelopService.LevelStaticData.Collect();
    }

    private void DrawButtonCollectLevelList()
    {
      if (GUILayout.Button("Collect level list from build settings."))
        DevelopService.LevelListCollector.Collect();
    }

    private void DrawButtonApple()
    {
      Texture image = AssetDatabase.LoadAssetAtPath<Texture>(Const.AssetPath.ButtonTexture.Apple);
      if (GUILayout.Button(image, GUILayout.Width(_createButtonSize), GUILayout.Height(_createButtonSize)))
      {
        DevelopService.Prefab.Create.AppleMarker();
        EditorSceneManager.MarkSceneDirty(SceneManager.GetActiveScene());
      }
    }

    private void DrawButtonBadApple()
    {
      Texture image = AssetDatabase.LoadAssetAtPath<Texture>(Const.AssetPath.ButtonTexture.BadApple);
      if (GUILayout.Button(image, GUILayout.Width(_createButtonSize), GUILayout.Height(_createButtonSize)))
      {
        DevelopService.Prefab.Create.BadAppleMarker();
        EditorSceneManager.MarkSceneDirty(SceneManager.GetActiveScene());
      }
    }

    private void DrawButtonScope()
    {
      Texture image = AssetDatabase.LoadAssetAtPath<Texture>(Const.AssetPath.ButtonTexture.Scope);
      if (GUILayout.Button(image, GUILayout.Width(_createButtonSize), GUILayout.Height(_createButtonSize)))
      {
        DevelopService.Prefab.Create.ScopeMarker();
        EditorSceneManager.MarkSceneDirty(SceneManager.GetActiveScene());
      }
    }

    private void DrawButtonScull()
    {
      Texture image = AssetDatabase.LoadAssetAtPath<Texture>(Const.AssetPath.ButtonTexture.Scull);
      if (GUILayout.Button(image, GUILayout.Width(_createButtonSize), GUILayout.Height(_createButtonSize)))
      {
        DevelopService.Prefab.Create.ScullMarker();
        EditorSceneManager.MarkSceneDirty(SceneManager.GetActiveScene());
      }
    }

    private void DrawButtonInitialPoint()
    {
      if (GUILayout.Button("Initial point.", GUILayout.Width(80), GUILayout.Height(30)))
      {
        DevelopService.Prefab.Create.InitialPoint();
        EditorSceneManager.MarkSceneDirty(SceneManager.GetActiveScene());
      }
    }

    private void DrawButtonTransferToNewLevelTrigger()
    {
      if (GUILayout.Button("Transfer to new level trigger.", GUILayout.Width(180), GUILayout.Height(30)))
      {
        DevelopService.Prefab.Create.TransferToNewLevelTriggerMarker();
        EditorSceneManager.MarkSceneDirty(SceneManager.GetActiveScene());
      }
    }

    private void DrawButtonDeadlyWall()
    {
      Texture image = AssetDatabase.LoadAssetAtPath<Texture>(Const.AssetPath.ButtonTexture.DeadlyWall);
      if (GUILayout.Button(image, GUILayout.Width(_createButtonSize), GUILayout.Height(_createButtonSize)))
      {
        DevelopService.Prefab.Create.DeadlyWall();
        EditorSceneManager.MarkSceneDirty(SceneManager.GetActiveScene());
      }
    }

    private void DrawButtonDoorAndKey()
    {
      Texture image = AssetDatabase.LoadAssetAtPath<Texture>(Const.AssetPath.ButtonTexture.Door);
      if (GUILayout.Button(image, GUILayout.Width(_createButtonSize), GUILayout.Height(_createButtonSize)))
      {
        DevelopService.Prefab.Create.DoorAndKey();
        EditorSceneManager.MarkSceneDirty(SceneManager.GetActiveScene());
      }
    }

    private void DrawButtonDoorEnemySnakeTrajectory()
    {
      Texture image = AssetDatabase.LoadAssetAtPath<Texture>(Const.AssetPath.ButtonTexture.EnemySnake);
      if (GUILayout.Button(image, GUILayout.Width(_createButtonSize), GUILayout.Height(_createButtonSize)))
      {
        DevelopService.Prefab.Create.EnemySnakeTrajectoryMarker();
        EditorSceneManager.MarkSceneDirty(SceneManager.GetActiveScene());
      }
    }

    private static LevelStaticData LevelStaticData() =>
      LevelStaticDataCollector.LevelStaticData(SceneManager.GetActiveScene().name);
  }
}