﻿using System.Collections.Generic;
using UnityEngine;

namespace StaticData
{
  [CreateAssetMenu(fileName = "NewLevelList", menuName = "Scriptable objects/LevelList static data")]
  public class LevelList : ScriptableObject
  {
    public List<string> PathsScenes;
  }
}