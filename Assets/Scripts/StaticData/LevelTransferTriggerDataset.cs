﻿using System;
using Data;
using UnityEngine;

namespace StaticData
{
  [Serializable]
  public class LevelTransferTriggerDataset
  {
    public Vector3Data PositionTrigger;
    public Vector3Data rotationTriggerTrigger;
    public Vector3Data ColliderCenter;
    public Vector3Data ColliderSize;

    public LevelTransferTriggerDataset(Vector3Data positionTrigger, Vector3Data rotationTrigger, Vector3Data colliderCenter, Vector3Data colliderSize)
    {
      PositionTrigger = positionTrigger;
      rotationTriggerTrigger = rotationTrigger;
      ColliderCenter = colliderCenter;
      ColliderSize = colliderSize;
    }

    public void Deconstruct(out Vector3 positionTrigger, out Vector3 rotationTrigger, out Vector3 colliderCenter, out Vector3 colliderSize)
    {
      positionTrigger = PositionTrigger.AsVector3();
      rotationTrigger = rotationTriggerTrigger.AsVector3();
      colliderCenter = ColliderCenter.AsVector3();
      colliderSize = ColliderSize.AsVector3();
    }
  }
}