using System;
using Data;
using Extensions;
using UnityEngine;

namespace StaticData
{
  [Serializable]
  public class PositionData
  {
    public Vector3Data Position;

    public PositionData(Vector3 position) =>
      Position = position.AsVector3Data();
  }
}