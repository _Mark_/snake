using System;
using Data;
using GameMechanics.SpawnerCollectibles;

namespace StaticData.CollectibleObjects
{
  [Serializable]
  public class Spawner
  {
    public Vector3Data FirstPoint;
    public Vector3Data SecondPoint;
    public SpawnType Spawns;
  }
}