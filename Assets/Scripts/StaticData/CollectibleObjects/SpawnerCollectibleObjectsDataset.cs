using System;

namespace StaticData.CollectibleObjects
{
  [Serializable]
  public class SpawnerCollectibleObjectsDataset
  {
    public Spawner Spawner;
    public Controller Controller;
  }
}