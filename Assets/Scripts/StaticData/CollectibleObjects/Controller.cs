using System;

namespace StaticData.CollectibleObjects
{
  [Serializable]
  public class Controller
  {
    public int StartQuantityObjects = 10;
    public int MaxQuantityObjects = 20;
    public int CurrentQuantityObjects = 0;
    public float SpawnTime = 2;
  }
}