﻿using System;
using System.Collections.Generic;
using Data;

namespace StaticData
{
  [Serializable]
  public class SpawnEnemySnakeDataset
  {
    public int LenghtTile;
    public float SpeedSnake;
    public int CurrentTarget;
    public float DistanceToChangedTargetPoint;
    public List<Vector3Data> Points;

    public SpawnEnemySnakeDataset(int lenghtTile, float speedSnake, int currentTarget, float distanceToChangedTargetPoint, List<Vector3Data> points)
    {
      LenghtTile = lenghtTile;
      SpeedSnake = speedSnake;
      CurrentTarget = currentTarget;
      DistanceToChangedTargetPoint = distanceToChangedTargetPoint;
      Points = points;
    }
  }
}