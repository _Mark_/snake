using System;

namespace StaticData
{
  [Serializable]
  public class TimeOfLevel
  {
    public float Value;
  }
}