﻿using System.Collections.Generic;
using UnityEngine;

namespace StaticData
{
  [CreateAssetMenu(fileName = "NewStaticData", menuName = "Scriptable objects/Level static data")]
  public class LevelStaticData : ScriptableObject
  {
    public string LevelKey;
    public TimeOfLevel TimeOfLevel;
    public LevelTransferTriggerDataset LevelTransferTriggerDataset;
    public StarsProgress StarsProgress;
    public List<SpawnEnemySnakeDataset> SpawnEnemySnakeDataset;
    public List<CollectibleObjects.SpawnerCollectibleObjectsDataset> SpawnerCollectibleObjects;
    public List<PositionData> ApplePositions;
    public List<PositionData> BadApplePositions;
    public List<PositionData> ScopePositions;
    public List<PositionData> SkullPositions;
  }
}