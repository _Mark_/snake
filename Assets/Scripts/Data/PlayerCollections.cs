﻿using System;

namespace Data
{
  [Serializable]
  public class PlayerCollections
  {
    public int Coins;

    public event Action Changed;

    public void Collect(int value)
    {
      Coins += value;
      Changed?.Invoke();
    }
  }
}