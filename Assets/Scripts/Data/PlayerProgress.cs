﻿using System;

namespace Data
{
  [Serializable]
  public class PlayerProgress
  {
    public PlayerCollections PlayerCollections;
    public LevelEvenNumber LevelNumber;
    public LastLevel LastLevel;

    public PlayerProgress()
    {
      PlayerCollections = new PlayerCollections();
      LevelNumber = new LevelEvenNumber();
      LastLevel = new LastLevel();
    }
  }
}