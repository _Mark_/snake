﻿using System;
using UnityEngine;

namespace Data
{
  [Serializable]
  public struct Vector3Data
  {
    public float X;
    public float Y;
    public float Z;

    public Vector3Data(float x, float y, float z)
    {
      X = x;
      Y = y;
      Z = z;
    }

    public Vector3Data(Vector3 vector)
    {
      X = vector.x;
      Y = vector.y;
      Z = vector.z;
    }

    public Vector3 AsVector3() =>
      new Vector3(X, Y, Z);
  }
}