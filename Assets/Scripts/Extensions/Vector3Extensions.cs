﻿using Data;
using UnityEngine;

namespace Extensions
{
  public static class Vector3Extensions
  {
    public static Vector3Data AsVector3Data(this Vector3 vector) =>
      new Vector3Data(vector);
  }
}