﻿using GameMechanics.Snake;
using GameMechanics.Snake.Movement;
using UnityEngine;

namespace GameMechanics
{
  public class BreakChainSnake : MonoBehaviour
  {
    public MovementTailChains MovementTailChainsPlayer;

    public void BreakOnElement(ChainTailSnake chainTailSnake)
    {
      for (int i = chainTailSnake.Number; i < MovementTailChainsPlayer.Chains.Count; i++)
      {
        GameObject chain = MovementTailChainsPlayer.Chains[i].gameObject;
        DisableLookAt(chain);
        SetRigidBodySettings(chain);
        Destroy(chain.GetComponent<ChainTailSnake>());
        StartTimerOfReturnToPool(chain);
      }
      RemoveRangeChains(chainTailSnake.Number);
    }

    public void BreakAwayLast()
    {
      int chainsCount = MovementTailChainsPlayer.ChainsCount;
      if (chainsCount > 1)
      {
        int lastNumber = chainsCount - 1;

        GameObject chain = MovementTailChainsPlayer.Chains[lastNumber].gameObject;
        DisableLookAt(chain);
        SetRigidBodySettings(chain);
        Destroy(chain.GetComponent<ChainTailSnake>());
        StartTimerOfReturnToPool(chain);
        RemoveRangeChains(lastNumber);
      }
    }

    private void RemoveRangeChains(int number)
    {
      MovementTailChainsPlayer.ChainsRemoveRange(number, MovementTailChainsPlayer.Chains.Count - number);
      MovementTailChainsPlayer.ChainPositionsRemoveRange(number, MovementTailChainsPlayer.ChainPositions.Count - number);
    }

    private static void StartTimerOfReturnToPool(GameObject chain) =>
      chain.GetComponent<DisablerGameObject>().enabled = true;

    private static void SetRigidBodySettings(GameObject chain)
    {
      Rigidbody rigidbody = chain.GetComponent<Rigidbody>();
      rigidbody.drag = 0;
      rigidbody.angularDrag = 0;
      rigidbody.useGravity = true;
      rigidbody.freezeRotation = false;
    }

    private static void DisableLookAt(GameObject chain) =>
      chain.GetComponent<LookAtTransform>().enabled = false;
  }
}