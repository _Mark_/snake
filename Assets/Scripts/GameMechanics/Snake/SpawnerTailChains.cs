﻿using GameMechanics.Snake.Movement;
using Services;
using Services.Assets;
using UnityEngine;

namespace GameMechanics.Snake
{
  public class SpawnerTailChains : MonoBehaviour, IService
  {
    public MovementTailChains MovementTailChains;

    private GameObject _chainTailSnakePrefab;

    public async void Construct(IAssetProvider assetProvider) =>
      _chainTailSnakePrefab = await assetProvider.LoadAsync<GameObject>(Const.ResourcesPaths.ChainTailSnake);

    public void SpawnChain()
    {
      GameObject chain = Instantiate(_chainTailSnakePrefab, LastChainPosition(), Quaternion.identity, SnakeHead());
      ChainTailSnake chainTailSnake = chain.GetComponent<ChainTailSnake>();

      SetTargetLookAt(chain);
      AddIntoMovementTailChains(chain);
      SetChainTailSnakeNumber(chainTailSnake);
      chain.SetActive(true);
    }

    private void SetTargetLookAt(GameObject chain) =>
      ChainLookAt(chain).Target = LastChain();

    private void AddIntoMovementTailChains(GameObject chain)
    {
      MovementTailChains.AddChain(chain.transform);
      MovementTailChains.AddChainPositions(chain.transform.position);
    }

    private void SetChainTailSnakeNumber(ChainTailSnake chainTailSnake) =>
      chainTailSnake.Number = MovementTailChains.Chains.Count - 1;

    private Transform LastChain() =>
      MovementTailChains.Chains[MovementTailChains.Chains.Count - 1];

    private Transform SnakeHead() =>
      MovementTailChains.Head;

    private Vector3 LastChainPosition() =>
      MovementTailChains.ChainPositions[MovementTailChains.ChainPositions.Count - 1];

    private static LookAtTransform ChainLookAt(GameObject chain) =>
      chain.GetComponent<LookAtTransform>();
  }
}