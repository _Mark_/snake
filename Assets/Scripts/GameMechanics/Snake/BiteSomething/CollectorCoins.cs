﻿using GameMechanics.Collectibles;
using Services.PersistentProgressService;

namespace GameMechanics.Snake.BiteSomething
{
  public class CollectorCoins : Collector
  {
    private IPersistentProgressService _progressService;

    public void Constructor(IPersistentProgressService progressService) =>
      _progressService = progressService;

    public override void Something(ICollectibles collectibles)
    {
      if (collectibles is Coin coin)
        CollectCoins(coin);
    }

    private void CollectCoins(Coin coin)
    {
      _progressService.PlayerProgress.PlayerCollections.Coins++;
      Destroy(coin.gameObject);
    }
  }
}