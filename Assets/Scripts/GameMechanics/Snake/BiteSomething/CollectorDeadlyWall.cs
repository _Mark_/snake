﻿using GameMechanics.Collectibles;

namespace GameMechanics.Snake.BiteSomething
{
  public class CollectorDeadlyWall : Collector
  {
    public SnakeDeath Death;

    public override void Something(ICollectibles collectibles)
    {
      if (collectibles is DeadlyWall deadlyWall)
        Collect(deadlyWall);
    }

    private void Collect(DeadlyWall deadlyWall) =>
      Death.KillSnake();
  }
}