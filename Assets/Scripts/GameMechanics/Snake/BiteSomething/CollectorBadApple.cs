﻿using GameMechanics.Collectibles;
using Services.Factories.Game;

namespace GameMechanics.Snake.BiteSomething
{
  public class CollectorBadApple : Collector
  {
    public BreakChainSnake BreakChainSnake;

    public override void Something(ICollectibles collectibles)
    {
      if (collectibles is BadApple badApple)
        Collect(badApple);
    }

    private void Collect(BadApple badApple)
    {
      BreakChainSnake.BreakAwayLast();
      badApple.gameObject.SetActive(false);
      if (badApple.SpawnController != null)
        badApple.SpawnController.InstantiateObject(TypeOfCreatingObject.BadApple);
    }
  }
}