﻿using GameMechanics.Collectibles;
using GameMechanics.EnemySnake;
using UnityEngine;

namespace GameMechanics.Snake.BiteSomething
{
  public class CollectorEnemySnakeTailChain : Collector
  {
    public override void Something(ICollectibles collectibles)
    {
      if (collectibles is EnemySnakeTailChain tail)
        BreakChainOnElement(tail);
    }

    private void BreakChainOnElement(EnemySnakeTailChain tailChain)
    {
      for (int i = tailChain.Number; i < tailChain.MovementTailChains.Chains.Count; i++)
      {
        GameObject chain = tailChain.MovementTailChains.Chains[i].gameObject;
        DisableLookAt(chain);
        SetRigidBodySettings(chain);
        Destroy(chain.GetComponent<EnemySnakeTailChain>());
        StartTimerOfReturnToPool(chain); 
      }

      tailChain.MovementTailChains.Chains.RemoveRange(tailChain.Number, tailChain.MovementTailChains.Chains.Count - tailChain.Number);
      tailChain.MovementTailChains.ChainPositions.RemoveRange(tailChain.Number, tailChain.MovementTailChains.ChainPositions.Count - tailChain.Number);
    }

    private static void StartTimerOfReturnToPool(GameObject chain) =>
      chain.GetComponent<ReturnToPoolEnemySnakeTailChain>().enabled = true;

    private static void SetRigidBodySettings(GameObject chain)
    {
      Rigidbody rigidbody = chain.GetComponent<Rigidbody>();
      rigidbody.drag = 0;
      rigidbody.angularDrag = 0;
      rigidbody.useGravity = true;
      rigidbody.freezeRotation = false;
    }

    private static void DisableLookAt(GameObject chain) =>
      chain.GetComponent<LookAtTransform>().enabled = false;
  }
}