﻿using GameMechanics.Collectibles;
using GameMechanics.DoorKey;

namespace GameMechanics.Snake.BiteSomething
{
  public class CollectorKeys : Collector
  {
    public PocketForKey Pocket;

    public override void Something(ICollectibles collectibles)
    {
      if (collectibles is Key key)
        Collect(key);
    }

    private void Collect(Key key)
    {
      Pocket.RaiseTheKey();
      Destroy(key.gameObject);
    }
  }
}