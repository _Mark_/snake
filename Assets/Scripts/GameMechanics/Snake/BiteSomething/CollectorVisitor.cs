﻿using System.Collections.Generic;
using GameMechanics.Collectibles;
using UnityEngine;

namespace GameMechanics.Snake.BiteSomething
{
  public class CollectorVisitor : MonoBehaviour
  {
    public TriggerDetector Detector = default;
    public List<Collector> Collectors;

    private void OnEnable() =>
      Detector.Detected += Collect;

    private void OnDisable() =>
      Detector.Detected -= Collect;

    private void Collect(Collider other)
    {
      if (other.gameObject.TryGetComponent<ICollectibles>(out ICollectibles collected))
        foreach (Collector collector in Collectors)
          collected.Collect(collector);
    }
  }
}