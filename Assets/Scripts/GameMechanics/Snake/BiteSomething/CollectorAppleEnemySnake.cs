﻿using GameMechanics.Collectibles;

namespace GameMechanics.Snake.BiteSomething
{
  public class CollectorAppleEnemySnake : Collector
  {
    public override void Something(ICollectibles collectibles)
    {
      if (collectibles is Apple apple)
        Destroy(apple.gameObject);
    }
  }
}