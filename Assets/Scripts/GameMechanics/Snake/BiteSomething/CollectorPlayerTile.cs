﻿using GameMechanics.Collectibles;

namespace GameMechanics.Snake.BiteSomething
{
  public class CollectorPlayerTile : Collector
  {
    public BreakChainSnake BreakChainSnake;

    public override void Something(ICollectibles collectibles)
    {
      if (collectibles is ChainTailSnake tail)
        BreakChainSnake.BreakOnElement(tail);
    }
  }
}