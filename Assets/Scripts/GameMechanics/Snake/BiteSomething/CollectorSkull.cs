﻿using GameMechanics.Collectibles;
using Services.Factories.Game;

namespace GameMechanics.Snake.BiteSomething
{
  public class CollectorSkull : Collector
  {
    public SnakeDeath Death;

    public override void Something(ICollectibles collectibles)
    {
      if (collectibles is Skull skull)
        Collect(skull);
    }

    private void Collect(Skull skull)
    {
      Death.KillSnake();
      skull.gameObject.SetActive(false);
      if (skull.SpawnController != null)
        skull.SpawnController.InstantiateObject(TypeOfCreatingObject.Skull);
    }
  }
}