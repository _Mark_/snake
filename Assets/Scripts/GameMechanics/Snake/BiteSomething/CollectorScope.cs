﻿using GameMechanics.Collectibles;
using GameMechanics.Snake.TargetDesignator;
using Services.Factories.Game;

namespace GameMechanics.Snake.BiteSomething
{
  public class CollectorScope : Collector
  {
    public ControllerTargetDesignatorLine Controller;

    public override void Something(ICollectibles collectibles)
    {
      if (collectibles is Scope scope)
        Collect(scope);
    }

    private void Collect(Scope scope)
    {
      Controller.EnableScopeOnTime(scope.ValidityPeriod);
      scope.gameObject.SetActive(false);
      if (scope.SpawnController != null)
        scope.SpawnController.InstantiateObject(TypeOfCreatingObject.Scope);
    }
  }
}