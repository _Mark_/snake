﻿using GameMechanics.Collectibles;
using UnityEngine;

namespace GameMechanics.Snake.BiteSomething
{
  public abstract class Collector : MonoBehaviour
  {
    public abstract void Something(ICollectibles collectibles);
  }
}