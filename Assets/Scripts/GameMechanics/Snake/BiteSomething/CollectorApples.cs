﻿using GameMechanics.Collectibles;
using Services.Factories.Game;
using UnityEngine;

namespace GameMechanics.Snake.BiteSomething
{
  public class CollectorApples : Collector
  {
    [SerializeField] private SpawnerTailChains spawnerTailChains = default;

    public override void Something(ICollectibles collectibles)
    {
      if (collectibles is Apple apple)
        Collect(apple);
    }

    private void Collect(Apple apple)
    {
      spawnerTailChains.SpawnChain();
      apple.gameObject.SetActive(false);
      if (apple.SpawnController != null)
        apple.SpawnController.InstantiateObject(TypeOfCreatingObject.Apple);
    }
  }
}