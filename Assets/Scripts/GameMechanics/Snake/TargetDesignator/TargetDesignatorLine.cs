﻿using UnityEngine;

namespace GameMechanics.Snake.TargetDesignator
{
  public class TargetDesignatorLine : MonoBehaviour
  {
    public Transform Source;
    public LineRenderer Line;

    private void Start() =>
      Line.positionCount = 2;

    public void SetLineTarget(Vector3 target)
    {
      Line.SetPosition(0, Source.position);
      Line.SetPosition(1, target);
    }
  }
}