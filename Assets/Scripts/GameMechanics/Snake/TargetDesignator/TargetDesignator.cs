﻿using UnityEngine;

namespace GameMechanics.Snake.TargetDesignator
{
  public class TargetDesignator : MonoBehaviour
  {
    private const int _indent = 1;

    public LayerMask Mask;
    public TargetDesignatorLine TargetDesignatorLine;
    [SerializeField] private GameObject _targetPrefab = default;
    [SerializeField] private Transform _source = default;
    [SerializeField] private Transform _camera = default;
    [SerializeField] private float _sizeTargetInstance = 1;

    private Transform _target;
    private GameObject _targetInstance;

    public Transform Target => _target;
    public bool IsActiveTargetInstance => _targetInstance.activeSelf;

    private void Awake()
    {
      _targetInstance = Instantiate(_targetPrefab);
      _target = _targetInstance.transform;
    }

    private void LateUpdate()
    {
      Ray ray = ForwardRay();
      if (Physics.Raycast(ray, out RaycastHit hit, Mathf.Infinity, Mask, QueryTriggerInteraction.Ignore))
      {
        ActivateTargetInstance();
        SetPositionTargetAtHit(hit);
        ResizeLocalScaleTargetInstance(hit);
        EnableLine();
        SetLineTarget(hit);
      }
      else
      {
        DisableTarget();
        TargetDesignatorLine.Line.enabled = false;
      }
    }

    public void DisableTarget() =>
      _targetInstance.SetActive(false);

    private void ActivateTargetInstance()
    {
      if (IsDisabledTargetInstance())
        _targetInstance.SetActive(true);
    }

    private void SetPositionTargetAtHit(RaycastHit hit) =>
      _target.position = hit.point;

    private void ResizeLocalScaleTargetInstance(RaycastHit hit)
    {
      float distanceScale = Vector3.Distance(_camera.position, hit.point);
      _target.localScale = Vector3.one * (_sizeTargetInstance * distanceScale);
    }

    private void EnableLine() =>
      TargetDesignatorLine.Line.enabled = true;

    private void SetLineTarget(RaycastHit hit) =>
      TargetDesignatorLine.SetLineTarget(hit.point);

    private Ray ForwardRay()
    {
      Vector3 forward = _source.forward;
      return new Ray(_source.position + forward * _indent, forward);
    }

    private bool IsDisabledTargetInstance() =>
      !_targetInstance.activeSelf;
  }
}