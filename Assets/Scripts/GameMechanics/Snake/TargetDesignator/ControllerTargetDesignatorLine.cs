﻿using System;
using UnityEngine;

namespace GameMechanics.Snake.TargetDesignator
{
  public class ControllerTargetDesignatorLine : MonoBehaviour
  {
    public TargetDesignator TargetDesignator;
    public TargetDesignatorLine TargetDesignatorLine;

    private float _validityPeriod;
    private float _timer;

    public event Action TimerEnabled;
    public event Action TimerDisabled;
    public event Action TimerUpdate;

    public float RemainingTime =>
      _validityPeriod - _timer;

    private void Update()
    {
      _timer += Time.deltaTime;
      if (_timer >= _validityPeriod)
        DisabledScope();
      TimerUpdate?.Invoke();
    }

    public void EnableScopeOnTime(float validityPeriod)
    {
      _validityPeriod = validityPeriod;
      _timer = 0;
      enabled = true;

      TargetDesignator.enabled = true;
      TargetDesignatorLine.Line.enabled = true;

      TimerEnabled?.Invoke();
    }

    private void DisabledScope()
    {
      enabled = false;
      _timer = 0;

      TargetDesignator.enabled = false;
      TargetDesignatorLine.Line.enabled = false;

      TargetDesignator.DisableTarget();

      TimerDisabled?.Invoke();
    }
  }
}