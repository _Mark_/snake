﻿using UnityEngine;

namespace GameMechanics.Snake
{
  public class DisablerGameObject : MonoBehaviour
  {
    public GameObject Chain;
    public float TimeBeforeReturn = 5;

    private float _timer = 0;

    private void Update()
    {
      _timer += Time.deltaTime;

      if (_timer >= TimeBeforeReturn)
      {
        ResetReturnToPool();
        Chain.SetActive(false);
      }
    }

    private void ResetReturnToPool()
    {
      enabled = false;
      _timer = 0;
    }
  }
}