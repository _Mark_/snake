﻿using UnityEngine;

namespace GameMechanics.Snake
{
  public class LookAtTransform : MonoBehaviour
  {
    public Transform Target;
    public Transform Executor;
    
    public void LateUpdate() =>
      Executor.LookAt(Target);
  }
}