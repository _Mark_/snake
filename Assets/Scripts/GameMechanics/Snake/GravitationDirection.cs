﻿using System.Collections;
using UnityEngine;

namespace GameMechanics.Snake
{
  [RequireComponent(typeof(Collider))]
  public class GravitationDirection : MonoBehaviour
  {
    public float TimeRotate;

    private void OnTriggerEnter(Collider other)
    {
      if (other.gameObject.CompareTag(Const.Tag.Player))
        StartCoroutine(SmoothRotatePlayer(other.gameObject.transform));
    }

    IEnumerator SmoothRotatePlayer(Transform player)
    {
      float timer = 0;
      Quaternion startRotation = player.rotation;

      while (timer < TimeRotate)
      {
        player.rotation = Quaternion.Slerp(startRotation, transform.rotation, timer / TimeRotate);
        timer += Time.deltaTime;
        yield return null;
      }

      player.transform.rotation = transform.rotation;
    }
  }
}