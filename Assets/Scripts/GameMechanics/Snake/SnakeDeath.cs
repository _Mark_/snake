﻿using System;
using System.Collections;
using Services.Level;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace GameMechanics.Snake
{
  public class SnakeDeath : MonoBehaviour
  {
    [SerializeField] private float _durationBeforeShowWindow = 2f;
    public BreakChainSnake BreakChainSnake;
    public Rigidbody Body;
    public Collider ColliderOfSnake;
    public PhysicMaterial DeadMaterial;
    public Transform PreviousCamera;
    public GameObject DeadCamera;
    public MonoBehaviour[] DisabledComponents;
    public GameObject[] DisabledObjects;

    private ILevelService _levelService;

    public event Action Died;

    public void Constructor(ILevelService levelService) =>
      _levelService = levelService;

    public void KillSnake()
    {
      DisableComponents();
      DisableObjects();
      BreakChainSnakeTile();
      EnableDiedCamera();
      EnablePhysics();
      StartCoroutine(InformLevelServiceAboutDeath());
      Died?.Invoke();
    }

    private IEnumerator InformLevelServiceAboutDeath()
    {
      yield return new WaitForSeconds(_durationBeforeShowWindow);
      _levelService.DeathSnake(SceneManager.GetActiveScene().path);
    }

    private void BreakChainSnakeTile()
    {
      if (BreakChainSnake.MovementTailChainsPlayer.Chains.Count > 1)
        BreakChainSnake.BreakOnElement(ChainTailSnake());

      ChainTailSnake ChainTailSnake() =>
        BreakChainSnake.MovementTailChainsPlayer.Chains[1].GetComponent<ChainTailSnake>();
    }

    private void EnableDiedCamera()
    {
      GameObject deadCamera = Instantiate(DeadCamera, PreviousCamera.position, PreviousCamera.rotation);
      deadCamera.GetComponent<LookAtTransform>().Target = Body.gameObject.transform;
    }

    private void EnablePhysics()
    {
      Body.useGravity = true;
      Body.freezeRotation = false;
      Body.drag = 0;
      ColliderOfSnake.material = DeadMaterial;
    }

    private void DisableObjects()
    {
      foreach (GameObject obj in DisabledObjects)
        obj.SetActive(false);
    }

    private void DisableComponents()
    {
      foreach (MonoBehaviour component in DisabledComponents)
        component.enabled = false;
    }
  }
}