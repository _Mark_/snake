﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameMechanics.Snake.Movement
{
  public class MovementTailChains : MonoBehaviour
  {
    public float DistanceBetweenChains = 1;

    public Transform Head;

    [SerializeField] private List<Transform> _chains = default;
    [SerializeField] private List<Vector3> _chainPositions = default;

    public event Action ChainChanged;
    public event Action ChainPositionChanged;
    public event Action ChainRemoved;

    public List<Transform> Chains => _chains;
    public List<Vector3> ChainPositions => _chainPositions;

    public int ChainsCount => _chains.Count;
    public int ChainsPositionsCount => _chainPositions.Count;

    private void Awake()
    {
      _chains.Add(Head);
      _chainPositions.Add(_chains[0].position);
    }

    public void Update()
    {
      Vector3 toHead = VectorToHead();
      float distance = toHead.magnitude;

      if (DistanceToHeadMoreThanBetweenChains(distance))
      {
        AddNewChainPosition(toHead);
        RemoveLastChainPosition();

        distance -= DistanceBetweenChains;
      }

      MoveTail(distance);
    }

    public void AddChain(Transform newChainTransform)
    {
      _chains.Add(newChainTransform);
      ChainChanged?.Invoke();
    }

    public void AddChainPositions(Vector3 newChainPositions)
    {
      _chainPositions.Add(newChainPositions);
      ChainPositionChanged?.Invoke();
    }

    public void ChainsRemoveRange(int number, int chainsCount)
    {
      // TODO: "chainsCount" sometimes low than 0;
      _chains.RemoveRange(number, chainsCount);
      ChainRemoved?.Invoke();
      ChainChanged?.Invoke();
    }

    public void ChainPositionsRemoveRange(int number, int chainPositionsCount)
    {
      _chainPositions.RemoveRange(number, chainPositionsCount);
      ChainPositionChanged?.Invoke();
    }

    private void MoveTail(float distance)
    {
      for (int i = 1; i < _chains.Count; i++)
        _chains[i].position =
          Vector3.Lerp(_chainPositions[i], _chainPositions[i - 1], distance / DistanceBetweenChains);
    }

    private void RemoveLastChainPosition() =>
      _chainPositions.RemoveAt(_chainPositions.Count - 1);

    private void AddNewChainPosition(Vector3 toHead) =>
      _chainPositions.Insert(0, NextChainPositions(toHead));

    private bool DistanceToHeadMoreThanBetweenChains(float distance) =>
      distance > DistanceBetweenChains;

    private Vector3 NextChainPositions(Vector3 toHead) =>
      _chainPositions[0] + toHead.normalized * DistanceBetweenChains;

    private Vector3 VectorToHead() =>
      Head.position - _chainPositions[0];
  }
}