﻿using GameMechanics.InputJoystick;
using UnityEngine;

namespace GameMechanics.Snake.Movement
{
  public class RotationHead : MonoBehaviour
  {
    [SerializeField] private float MinOffsetX = -90;
    [SerializeField] private float MaxOffsetX = 90;

    [SerializeField] private bool _inversionAxisX = false;
    [SerializeField] private bool _inversionAxisY = false;

    [SerializeField] private float _sensitivityAxisX = 1;
    [SerializeField] private float _sensitivityAxisY = 1;

    private float _xOffset;
    private float _yOffset;

    private readonly Quaternion _startingOrientation = Quaternion.identity;    

    public InputDrag InputService { get; set; }

    private void Start() =>
      SetStartOffsetRotation();

    public void Update() =>
      Rotate(YOffset(), XOffset());

    private float YOffset() =>
      FormingOffsetsY() * _sensitivityAxisY * Time.deltaTime;

    private float XOffset() =>
      FormingOffsetsX() * _sensitivityAxisX * Time.deltaTime;

    private void Rotate(float xOffset, float yOffset)
    {
      _xOffset += xOffset;
      _yOffset += yOffset;
      _xOffset = Mathf.Clamp(_xOffset, MinOffsetX, MaxOffsetX);
      transform.localRotation = _startingOrientation * Quaternion.Euler(-_xOffset, _yOffset, 0);
    }

    private void SetStartOffsetRotation()
    {
      Vector3 localEulerAngles = transform.localEulerAngles;
      _xOffset = -localEulerAngles.x;
      _yOffset = localEulerAngles.y;
    }

    private float FormingOffsetsY() =>
      _inversionAxisY
        ? -InputService.Drag.y
        : InputService.Drag.y;


    private float FormingOffsetsX() =>
      _inversionAxisX
        ? -InputService.Drag.x
        : InputService.Drag.x;
  }
}