﻿using UnityEngine;

namespace GameMechanics.Snake.Movement
{
  public class MoveForwardKinematicBody : MonoBehaviour
  {
    public Transform Head;
    public Rigidbody Body;
    public float Speed;

    public void FixedUpdate() =>
      Body.velocity = Head.forward * Speed;
  }
}