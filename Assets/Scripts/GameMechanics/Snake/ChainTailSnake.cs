﻿using GameMechanics.Collectibles;
using GameMechanics.Snake.BiteSomething;
using UnityEngine;

namespace GameMechanics.Snake
{
  public class ChainTailSnake : MonoBehaviour, ICollectibles
  {
    [SerializeField] private int _number;

    public int Number
    {
      get => _number;
      set => _number = value;
    }

    public void Collect(Collector collect)
    {
      collect.Something(this);
    }
  }
}