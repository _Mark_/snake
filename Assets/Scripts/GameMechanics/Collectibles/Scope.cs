﻿using GameMechanics.Snake.BiteSomething;
using GameMechanics.SpawnerCollectibles;
using UnityEngine;

namespace GameMechanics.Collectibles
{
  public class Scope : MonoBehaviour, ICollectibles
  {
    public ControllerSpawnerCollectibleObjects SpawnController;
    
    [SerializeField] private float _validityPeriod = default;
    
    public float ValidityPeriod => _validityPeriod;

    public void Collect(Collector collect) =>
      collect.Something(this);
  }
}