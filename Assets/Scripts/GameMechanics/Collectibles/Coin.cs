﻿using GameMechanics.Snake.BiteSomething;
using UnityEngine;

namespace GameMechanics.Collectibles
{
  public class Coin : MonoBehaviour, ICollectibles
  {
    public void Collect(Collector collect) =>
      collect.Something(this);
  }
}