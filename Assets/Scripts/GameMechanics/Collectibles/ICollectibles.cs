﻿using GameMechanics.Snake.BiteSomething;
using UnityEngine;

namespace GameMechanics.Collectibles
{
  public interface ICollectibles
  {
    GameObject gameObject { get; }
    void Collect(Collector collect);
  }
}