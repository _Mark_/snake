﻿using GameMechanics.Snake.BiteSomething;
using GameMechanics.SpawnerCollectibles;
using UnityEngine;

namespace GameMechanics.Collectibles
{
  public class BadApple : MonoBehaviour, ICollectibles
  {
    public ControllerSpawnerCollectibleObjects SpawnController;
    
    public void Collect(Collector collect) =>
      collect.Something(this);
  }
}