﻿using System;
using UnityEngine;

namespace GameMechanics
{
  [RequireComponent(typeof(Collider))]
  public class TriggerDetector : MonoBehaviour
  {
    public event Action<Collider> Detected;

    private void OnTriggerEnter(Collider other) =>
      Detected?.Invoke(other);
  }
}