﻿using UnityEngine;

namespace GameMechanics.DoorKey
{
  public class PocketForKey : MonoBehaviour
  {
    [SerializeField] private bool _availabilityOfKey;
    
    public bool AvailabilityOfKey => _availabilityOfKey;

    public void RaiseTheKey() =>
      _availabilityOfKey = true;

    public void WithdrawTheKey() =>
      _availabilityOfKey = false;
  }
}