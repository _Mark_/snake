﻿using UnityEngine;
using UnityEngine.Events;

namespace GameMechanics.DoorKey
{
  public class Padlock : MonoBehaviour
  {
    public TriggerDetector Detector;

    public UnityEvent OpenThePadlock;

    private void OnEnable() =>
      Detector.Detected += Detected;

    private void OnDisable() =>
      Detector.Detected -= Detected;

    private void Detected(Collider other)
    {
      if (other.TryGetComponent<PocketForKey>(out PocketForKey pocket))
        if (pocket.AvailabilityOfKey)
          OpenTheDoor(pocket);
    }

    private void OpenTheDoor(PocketForKey pocket)
    {
      pocket.WithdrawTheKey();
      OpenThePadlock?.Invoke();
    }
  }
}