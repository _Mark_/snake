using System.Collections;
using GameMechanics.Snake.Movement;
using UnityEngine;

namespace GameMechanics
{
  public class SpeedBoost : MonoBehaviour
  {
    [SerializeField] private int _speed = 20;

    private IEnumerator Start()
    {
      yield return new WaitForSeconds(1);
      GameObject.FindWithTag(Const.Tag.Player).GetComponent<MoveForwardKinematicBody>().Speed = _speed;
    }
  }
}