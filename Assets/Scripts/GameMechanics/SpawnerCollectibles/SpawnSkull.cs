﻿using GameMechanics.Collectibles;
using UnityEngine;

namespace GameMechanics.SpawnerCollectibles
{
  public class SpawnSkull : SpawnOption
  {
    public override void Spawn(Transform parent, Vector3 position, ControllerSpawnerCollectibleObjects controller)
    {
      GameObject appleObject = Instantiate(_prefab, position, Quaternion.identity, parent);
      Skull skull = appleObject.GetComponent<Skull>();

      skull.SpawnController = controller == null
        ? null
        : controller;
    }
  }
}