﻿using GameMechanics.Collectibles;
using UnityEngine;

namespace GameMechanics.SpawnerCollectibles
{
  public class SpawnBadApple : SpawnOption
  {
    public override void Spawn(Transform parent, Vector3 position, ControllerSpawnerCollectibleObjects controller)
    {
      GameObject appleObject = Instantiate(_prefab, position, Quaternion.identity, parent);
      BadApple badApple = appleObject.GetComponent<BadApple>();

      badApple.SpawnController = controller == null
        ? null
        : controller;
    }
  }
}