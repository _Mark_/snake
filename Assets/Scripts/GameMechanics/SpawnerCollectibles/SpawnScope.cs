﻿using GameMechanics.Collectibles;
using UnityEngine;

namespace GameMechanics.SpawnerCollectibles
{
  public class SpawnScope : SpawnOption
  {
    public override void Spawn(Transform parent, Vector3 position, ControllerSpawnerCollectibleObjects controller)
    {
      GameObject appleObject = Instantiate(_prefab, position, Quaternion.identity, parent);
      Scope scope = appleObject.GetComponent<Scope>();

      scope.SpawnController = controller == null
        ? null
        : controller;
    }
  }
}