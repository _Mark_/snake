﻿using GameMechanics.Collectibles;
using UnityEngine;

namespace GameMechanics.SpawnerCollectibles
{
  public class SpawnApple : SpawnOption
  {
    public override void Spawn(
      Transform parent,
      Vector3 position,
      ControllerSpawnerCollectibleObjects controller = null)
    {
      GameObject appleObject = Instantiate(_prefab, position, Quaternion.identity, parent);
      Apple apple = appleObject.GetComponent<Apple>();

      apple.SpawnController = controller == null
        ? null
        : controller;
    }
  }
}