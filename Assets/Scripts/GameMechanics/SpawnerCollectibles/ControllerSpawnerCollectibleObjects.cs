﻿using System.Collections.Generic;
using Services.Factories.Game;
using UnityEngine;

namespace GameMechanics.SpawnerCollectibles
{
  public class ControllerSpawnerCollectibleObjects : MonoBehaviour
  {
    public SpawnerCollectibleObjects Spawner;
    public int MaxQuantityObjects = 600;
    
    private float _timer;
    private Dictionary<TypeOfCreatingObject, int> _createdObjects;

    public void Construct(
      SpawnerCollectibleObjects spawner,
      int maxQuantityObjects)
    {
      Spawner = spawner;
      MaxQuantityObjects = maxQuantityObjects;
    }

    private void Start() =>
      StartGenerateObjects();

    public void InstantiateObject(TypeOfCreatingObject type)
    {
      Spawner.SpawnPrefab(type);
    }

    private void StartGenerateObjects()
    {
      _createdObjects = new Dictionary<TypeOfCreatingObject, int>
      {
        {TypeOfCreatingObject.Apple, PercentCount(40)},
        {TypeOfCreatingObject.BadApple, PercentCount(30)},
        {TypeOfCreatingObject.Scope, PercentCount(20)},
        {TypeOfCreatingObject.Skull, PercentCount(10)},
      };

      foreach (KeyValuePair<TypeOfCreatingObject, int> createdObject in _createdObjects)
        for (int i = 0; i < createdObject.Value; i++)
          InstantiateObject(createdObject.Key);
    }

    private int PercentCount(int percents) =>
      MaxQuantityObjects / 100 * percents;
  }
}