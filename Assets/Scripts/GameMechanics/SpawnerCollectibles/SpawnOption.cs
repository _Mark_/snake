﻿using Services.Factories.Game;
using UnityEngine;

namespace GameMechanics.SpawnerCollectibles
{
  public abstract class SpawnOption : MonoBehaviour
  {
    [SerializeField] private TypeOfCreatingObject _type;
    [SerializeField] protected GameObject _prefab;

    public TypeOfCreatingObject Type => _type;
    public GameObject Prefab
    {
      set => _prefab = value;
    }

    public abstract void Spawn(Transform parent, Vector3 position, ControllerSpawnerCollectibleObjects controller);
  }
}