using UnityEngine;

namespace GameMechanics.SpawnerCollectibles
{
  public class SpawnerCollectibleObjectsMarker : MonoBehaviour
  {
    public Vector3 FirstPoint;
    public Vector3 SecondPoint;
    public SpawnType Spawns;
    
    public int StartQuantityObjects = 10;
    public int MaxQuantityObjects = 20;
    public int CurrentQuantityObjects = 0;
    public float SpawnTime = 2;
  }
}