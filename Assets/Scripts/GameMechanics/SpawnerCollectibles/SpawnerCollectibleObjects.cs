﻿using System.Collections.Generic;
using Services.Factories.Game;
using UnityEngine;
using Random = UnityEngine.Random;

namespace GameMechanics.SpawnerCollectibles
{
  public class SpawnerCollectibleObjects : MonoBehaviour
  {
    public ControllerSpawnerCollectibleObjects Controller;
    public Transform Parent;
    public Vector3 FirstPoint;
    public Vector3 SecondPoint;
    public SpawnOption[] SpawnOptions;
    
    private Dictionary<TypeOfCreatingObject, SpawnOption> _spawners = new Dictionary<TypeOfCreatingObject, SpawnOption>();

    public void Constructor(
      ControllerSpawnerCollectibleObjects controller,
      Vector3 firstPoint,
      Vector3 secondPoint,
      SpawnOption[] spawnOptions)
    {
      Controller = controller;
      Parent = transform;
      FirstPoint = firstPoint;
      SecondPoint = secondPoint;
      SpawnOptions = spawnOptions;

      foreach (SpawnOption spawn in SpawnOptions) 
        _spawners[spawn.Type] = spawn;
    }

    public void SpawnRandomPrefab()
    {
      SpawnOption spawn = SpawnOptions[Random.Range(0, SpawnOptions.Length)];
      spawn.Spawn(Parent, RandomPosition(), Controller);
    }

    public void SpawnPrefab(TypeOfCreatingObject typeObject)
    {
      SpawnOption spawn = _spawners[typeObject];
      spawn.Spawn(Parent, RandomPosition(), Controller);
    }

    
    private Vector3 RandomPosition()
    {
      Vector3 position = transform.position;
      float x = Random.Range(FirstPoint.x, SecondPoint.x) + position.x;
      float y = Random.Range(FirstPoint.y, SecondPoint.y) + position.y;
      float z = Random.Range(FirstPoint.z, SecondPoint.z) + position.z;

      return new Vector3(x, y, z);
    }
  }
}