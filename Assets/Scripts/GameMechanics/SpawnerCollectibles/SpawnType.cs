using System;

namespace GameMechanics.SpawnerCollectibles
{
  [Flags]
  public enum SpawnType
  {
    None,
    Apple,
    Skull,
    Scope,
    BadApple
  }
}