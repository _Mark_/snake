﻿using GameMechanics.Snake.Movement;
using Services.Level;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace GameMechanics.TransferTrigger
{
  public class LevelTransferTrigger : MonoBehaviour
  {
    [SerializeField] private BoxCollider _boxCollider = default;
    [SerializeField] private bool _triggered;
    
    private ILevelService _levelService;

    public void Constructor(Vector3 center, Vector3 size, ILevelService levelService)
    {
      _boxCollider.center = center;
      _boxCollider.size = size;
      _levelService = levelService;
    }

    private void OnTriggerEnter(Collider other)
    {
      if (!_triggered)
      {
        _triggered = true;
        StopMoveSnake(other);
        _levelService.EndLevel(SceneManager.GetActiveScene().path);
      }
    }

    private static void StopMoveSnake(Collider other) =>
      other.gameObject.GetComponent<MoveForwardKinematicBody>().Speed = 0;
  }
}