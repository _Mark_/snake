﻿using UnityEngine;

namespace GameMechanics.InputJoystick
{
  public abstract class InputDrag : MonoBehaviour, IInputDrag
  {
    public abstract Vector2 Drag { get; protected set; }
  }
}