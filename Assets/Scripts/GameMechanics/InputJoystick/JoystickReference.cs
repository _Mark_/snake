﻿using UnityEngine;

namespace GameMechanics.InputJoystick
{
  public class JoystickReference : MonoBehaviour
  {
    [SerializeField] private Joystick _joystick = null;
    public Joystick Reference => _joystick;
  }
}