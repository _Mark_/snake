﻿using UnityEngine;

namespace GameMechanics.InputJoystick
{
  // Отслеживание перемещения мыши (пальца) по экрану.
  public sealed class DragHoldMouse : InputDrag
  {

    private Vector2 _startPos = new Vector2();

    private Vector2 _newPos = new Vector2();

    public override Vector2 Drag { get; protected set; }

    public void UpdateOffset()
    {
      if (Input.GetMouseButtonDown(0))
      {
        _startPos = Input.mousePosition;
      }

      if (Input.GetMouseButton(0))
      {
        _newPos = Input.mousePosition;
        Drag = _newPos - _startPos;

        _startPos = Input.mousePosition;
      }
    }
  }
}