﻿using UnityEngine;

namespace GameMechanics.InputJoystick
{
  public interface IInputDrag
  {
    Vector2 Drag { get; }
  }
}