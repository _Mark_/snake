﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace GameMechanics.InputJoystick
{
  public class Joystick : InputDrag, IDragHandler, IPointerDownHandler, IPointerUpHandler
  {
    public GameObject WorkArea;
    public GameObject Background;
    public GameObject Stick;
    [SerializeField] private Vector2 _inputVector;

    private float _radiusInSquared;
    private Vector2 _center;
    private RectTransform _workAreaRect;
    private RectTransform _backgroundRect;
    private RectTransform _stickRect;

    public override Vector2 Drag { get; protected set; }

    private void Awake()
    {
      GetRectTransforms();
      CalculateRadiusInSquared();
      DisableJoystick();
    }

    public void Update() =>
      Drag = _inputVector;

    public void OnDrag(PointerEventData eventData)
    {
      if (RectTransformUtility
        .ScreenPointToLocalPointInRectangle(
          rect: _backgroundRect, screenPoint: eventData.position,
          cam: eventData.pressEventCamera, localPoint: out Vector2 point))
      {
        SetPositionStick(point);
        CalculateInputVector(point);
      }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
      if (RectTransformUtility
        .ScreenPointToLocalPointInRectangle(rect: _workAreaRect, screenPoint: eventData.position, cam: eventData.pressEventCamera, localPoint: out _center))
      {
        EnableJoystick();
        JoystickSetTapPointPosition();
        OnDrag(eventData);
      }
    }

    public void OnPointerUp(PointerEventData eventData)
    {
      DisableJoystick();
      _inputVector = Vector3.zero;
    }

    private void SetPositionStick(Vector2 point) =>
      _stickRect.anchoredPosition = (point.x * point.x - _center.x) + (point.y * point.y - _center.y) <= _radiusInSquared
        ? _center + point
        : PositionOnCircleBackground(point);

    private void CalculateInputVector(Vector2 point)
    {
      _inputVector = point / RadiusBackground();
      if (_inputVector.SqrMagnitude() > 1)
        _inputVector.Normalize();
    }

    private Vector2 PositionOnCircleBackground(Vector2 point) =>
      _center + point.normalized * RadiusBackground();

    private void CalculateRadiusInSquared()
    {
      float radius = RadiusBackground();
      _radiusInSquared = radius * radius;
    }

    private float RadiusBackground() =>
      _backgroundRect.sizeDelta.x / 2;

    private void GetRectTransforms()
    {
      _workAreaRect = WorkArea.GetComponent<RectTransform>();
      _backgroundRect = Background.GetComponent<RectTransform>();
      _stickRect = Stick.GetComponent<RectTransform>();
    }

    private void JoystickSetTapPointPosition()
    {
      _backgroundRect.anchoredPosition = _center;
      _stickRect.anchoredPosition = _center;
    }

    private void DisableJoystick()
    {
      Background.SetActive(false);
      Stick.SetActive(false);
    }

    private void EnableJoystick()
    {
      Background.SetActive(true);
      Stick.SetActive(true);
    }

  }
}