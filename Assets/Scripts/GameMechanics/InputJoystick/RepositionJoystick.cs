﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace GameMechanics.InputJoystick
{
  public class RepositionJoystick : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
  {
    public GameObject Joystick;
    public RectTransform PressArea;

    private RectTransform _joystickTransform;

    private void Start()
    {
      Joystick.SetActive(false);
      _joystickTransform = Joystick.GetComponent<RectTransform>();
    }
    
    public void OnPointerDown(PointerEventData eventData)
    {
      if (RectTransformUtility
        .ScreenPointToLocalPointInRectangle(rect: PressArea, screenPoint: eventData.position, cam: eventData.pressEventCamera, localPoint: out Vector2 point))
      {
        Joystick.SetActive(true);
        _joystickTransform.anchoredPosition = point;
      }
    }

    public void OnPointerUp(PointerEventData eventData)
    {
      Joystick.SetActive(false);
    }
  }
}