using System.Collections;
using UnityEngine;

namespace GameMechanics.TimerForLevel
{
  public class DisableTimer : MonoBehaviour
  {
    private IEnumerator Start()
    {
      yield return new WaitForSeconds(1f);
      GameObject.Find("TimerOfLevel").SetActive(false);
    }
  }
}