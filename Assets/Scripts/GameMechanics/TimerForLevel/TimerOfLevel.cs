using System;
using UnityEngine;

namespace GameMechanics.TimerForLevel
{
  public class TimerOfLevel : MonoBehaviour
  {
    [SerializeField] private float _current = 0;
    [SerializeField] private float _timeOnLevel = 60;

    public event Action EndTime;
    public event Action Changed;
    public event Action RestartTimer;

    public float RemainingTime =>
      (_timeOnLevel - _current > 0) ?
        _timeOnLevel - _current :
        0;

    private void FixedUpdate() =>
      TimeProgress();

    private void TimeProgress()
    {
      _current += Time.deltaTime;
      Changed?.Invoke();
      if (_current >= _timeOnLevel)
        EndTime?.Invoke();
    }

    public void AddSeconds(float amount)
    {
      _current = 0;
      _timeOnLevel = amount;
      RestartTimer?.Invoke();
    }
  }
}