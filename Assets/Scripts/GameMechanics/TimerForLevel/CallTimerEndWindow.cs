using Services.Windows;
using UnityEngine;

namespace GameMechanics.TimerForLevel
{
  public class CallTimerEndWindow : MonoBehaviour
  {
    [SerializeField] private TimerOfLevel _timer = default;
    private IWindowsService _windowsService;

    public void Construct(IWindowsService windowsService) =>
      _windowsService = windowsService;

    private void OnEnable() =>
      _timer.EndTime += Call;

    private void OnDisable() =>
      _timer.EndTime -= Call;

    private void Call() =>
      _windowsService.Open(WindowType.TimeEndWindow);
  }
}