using UnityEngine;

namespace GameMechanics.TimerForLevel
{
  public class FreezeTimeScale : MonoBehaviour
  {
    private const int _normalTimeScale = 1;

    [SerializeField] private float _freezeTimeScale = 0;
    [SerializeField] private TimerOfLevel _timer = default;

    private void OnEnable()
    {
      _timer.EndTime += Freeze;
      _timer.RestartTimer += UnFreeze;
    }

    private void OnDisable()
    {
      _timer.EndTime -= Freeze;
      _timer.RestartTimer -= UnFreeze;
    }

    private void OnDestroy() =>
      UnFreeze();

    private void Freeze() =>
      Time.timeScale = _freezeTimeScale;

    private void UnFreeze() =>
      Time.timeScale = _normalTimeScale;
  }
}