﻿using GameMechanics.Collectibles;
using GameMechanics.Snake.BiteSomething;
using GameMechanics.Snake.Movement;
using UnityEngine;

namespace GameMechanics.EnemySnake
{
  public class EnemySnakeTailChain : MonoBehaviour, ICollectibles
  {
    public MovementTailChains MovementTailChains;    
    public int Number;

    public void Collect(Collector collect) =>
      collect.Something(this);
  }
}