﻿using UnityEngine;

namespace GameMechanics.EnemySnake
{
  public class MoveEnemySnakeHead : MonoBehaviour
  {
    public Rigidbody Body;
    public float Speed;

    public void FixedUpdate() =>
      Body.velocity = Body.transform.forward * Speed;
  }
}