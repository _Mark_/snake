﻿using UnityEngine;

namespace GameMechanics.EnemySnake
{
  public class SpawnEnemySnakeMarker : MonoBehaviour
  {
    public LookAtPositionControllerMarker Controller;
    public int LenghtTile = 10;
    public float SpeedSnake = 5f;
  }
}