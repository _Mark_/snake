﻿using UnityEngine;

namespace GameMechanics.EnemySnake
{
  public class LookAtPosition : MonoBehaviour
  {
    [SerializeField] private Vector3 _target = default;
    [SerializeField] private Rigidbody _body = default;

    public Vector3 Target
    {
      get => _target;
      set => _target = value;
    }

    public void FixedUpdate() =>
      _body.rotation = Quaternion.Slerp(_body.rotation, Quaternion.LookRotation(ToTarget()), Time.deltaTime);

    private Vector3 ToTarget() =>
      Target - _body.position;
  }
}