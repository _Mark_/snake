﻿using System.Collections.Generic;
using UnityEngine;

namespace GameMechanics.EnemySnake
{
  public class LookAtPositionController : MonoBehaviour
  {
    public LookAtPosition LookAtPosition;
    public Transform Executor;
    public int CurrentTarget = 0;
    public float DistanceToChangedTargetPoint = 1;

    public List<Vector3> Points;
    
    public Vector3 CurrentPoint => Points[CurrentTarget]; 

    private void Start()
    {
     CheckPointsCountEqualsZero();
     SetCurrentTarget();
    }

    private void FixedUpdate()
    {
      if (CanChangedCurrentPointTarget())
      {
        UpdateCurrentTargetNumber();
        SetCurrentTarget();
      }
    }

    private bool CanChangedCurrentPointTarget() =>
      Vector3.Distance(Executor.position, Points[CurrentTarget]) < DistanceToChangedTargetPoint;

    private void UpdateCurrentTargetNumber()
    {
      if (++CurrentTarget >= Points.Count)
        CurrentTarget = 0;
    }

    private void SetCurrentTarget() =>
      LookAtPosition.Target = Points[CurrentTarget];

    private void CheckPointsCountEqualsZero()
    {
      if (Points.Count == 0)
        Debug.LogError("Points count equals: \"0\".");
    }
  }
}