﻿using System.Collections.Generic;
using UnityEngine;

namespace GameMechanics.EnemySnake
{
  public class LookAtPositionControllerMarker : MonoBehaviour
  {
    public int CurrentTarget = 0;
    public float DistanceToChangedTargetPoint = 1;

    public List<Vector3> Points;
  }
}