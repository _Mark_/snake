﻿using System.Threading.Tasks;
using GameMechanics.Snake;
using GameMechanics.Snake.Movement;
using Services.Assets;
using UnityEngine;

namespace GameMechanics.EnemySnake
{
  public class SpawnerEnemyTailChains : MonoBehaviour
  {
    public MovementTailChains MovementTailChains;

    private IAssetProvider _assetProvider;

    public void Constructor(IAssetProvider assetProvider) =>
      _assetProvider = assetProvider;

    public async Task CreateChainAsync()
    {
      GameObject chain = await _assetProvider.InstantiateAtAsync(Const.ResourcesPaths.EnemySnakeTailChain,
        LastChainPosition(), Quaternion.identity);
      EnemySnakeTailChain tailChain = chain.GetComponent<EnemySnakeTailChain>();

      chain.transform.SetParent(SnakeHead());
      SetMovementTailChains(chain);
      SetTargetLookAt(chain);
      AddIntoMovementTailChains(chain);
      SetEnemyTailChainNumber(tailChain);
      chain.SetActive(true);
    }

    private void SetMovementTailChains(GameObject chain) =>
      chain.GetComponent<EnemySnakeTailChain>().MovementTailChains = MovementTailChains;

    private void SetTargetLookAt(GameObject chain) =>
      ChainLookAt(chain).Target = LastChain();

    private void AddIntoMovementTailChains(GameObject chain)
    {
      MovementTailChains.AddChain(chain.transform);
      MovementTailChains.AddChainPositions(chain.transform.position);
    }

    private void SetEnemyTailChainNumber(EnemySnakeTailChain tailChain) =>
      tailChain.Number = MovementTailChains.ChainsCount - 1;

    private static LookAtTransform ChainLookAt(GameObject chain) =>
      chain.GetComponent<LookAtTransform>();

    private Transform LastChain() =>
      MovementTailChains.Chains[MovementTailChains.ChainsCount - 1];

    private Transform SnakeHead() =>
      MovementTailChains.Head;

    private Vector3 LastChainPosition() =>
      MovementTailChains.ChainPositions[MovementTailChains.ChainsPositionsCount - 1];
  }
}