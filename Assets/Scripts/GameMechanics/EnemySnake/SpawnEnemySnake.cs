﻿using System.Threading.Tasks;
using Services.Factories.Game;
using UnityEngine;

namespace GameMechanics.EnemySnake
{
  public class SpawnEnemySnake : MonoBehaviour
  {
    public LookAtPositionController Controller;
    public int LenghtTile = 10;
    public float SpeedSnake = 5f;

    private IGameFactory _factory;

    public void Constructor(IGameFactory factory) =>
      _factory = factory;

    public async Task<GameObject> Spawn() =>
      await _factory.CreateEnemySnake(Controller.CurrentPoint, Quaternion.identity, SpeedSnake);
  }
}