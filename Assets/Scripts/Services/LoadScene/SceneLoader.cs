﻿using System;
using System.Collections;
using Infrastructure;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Services.LoadScene
{
  public class SceneLoader : ISceneLoader
  {
    private readonly ICoroutineRunner _coroutineRunner;

    public SceneLoader(ICoroutineRunner coroutineRunner) =>
      _coroutineRunner = coroutineRunner;


    public void Load(string nameScene, Action onLoaded = null) =>
      _coroutineRunner.StartCoroutine(LoadScene(nameScene, onLoaded));

    public void LoadSingle(string nameScene, Action onLoaded = null) =>
      _coroutineRunner.StartCoroutine(LoadSceneSingle(nameScene, onLoaded));

    private IEnumerator LoadSceneSingle(string name, Action onLoaded)
    {
      AsyncOperation waitNextScene = SceneManager.LoadSceneAsync(name);

      while (!waitNextScene.isDone)
        yield return null;

      onLoaded?.Invoke();
    }

    private IEnumerator LoadScene(string name, Action onLoaded = null)
    {
      if (SceneManager.GetActiveScene().name == name)
      {
        onLoaded?.Invoke();
        yield break;
      }

      AsyncOperation waitNextScene = SceneManager.LoadSceneAsync(name);

      while (!waitNextScene.isDone)
        yield return null;

      onLoaded?.Invoke();
    }
  }
}