﻿using System;

namespace Services.LoadScene
{
  public interface ISceneLoader : IService
  {
    void Load(string nameScene, Action onLoaded = null);
    void LoadSingle(string nameScene, Action onLoaded = null);
  }
}