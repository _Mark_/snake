﻿using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace Services.Assets
{
  public class AssetProvider : IAssetProvider
  {
    private readonly Dictionary<string, AsyncOperationHandle<object>> _completedCached =
      new Dictionary<string, AsyncOperationHandle<object>>();

    public void Initialize() =>
      Addressables.InitializeAsync();

    public async Task<GameObject> InstantiateAsync(string address)
    {
      GameObject prefab = await LoadAsync<GameObject>(address);
      return Object.Instantiate(prefab);
    }

    public async Task<GameObject> InstantiateAtAsync(string address, Vector3 at, Quaternion rotation)
    {
      GameObject prefab = await LoadAsync<GameObject>(address);
      return Object.Instantiate(prefab, at, rotation);
    }

    public async Task<GameObject> InstantiateAtAsync(string address, Transform at)
    {
      GameObject prefab = await LoadAsync<GameObject>(address);
      return Object.Instantiate(prefab, at.position, at.rotation);
    }

    public async Task<T> LoadAsync<T>(string address) where T : class
    {
      if (_completedCached.TryGetValue(address, out AsyncOperationHandle<object> handle))
        return await HandleResult<T>(handle);

      AsyncOperationHandle<object> newHandle = Addressables.LoadAssetAsync<object>(address);
      _completedCached[address] = newHandle;

      return await newHandle.Task as T;
    }

    private static async Task<T> HandleResult<T>(AsyncOperationHandle<object> handle) where T : class
    {
      if (handle.IsDone)
        return handle.Result as T;

      return await handle.Task as T;
    }

    public TObject Instantiate<TObject>(string path)
      where TObject : Object =>
      Object.Instantiate<TObject>(LoadResource<TObject>(path));

    public TObject InstantiateAt<TObject>(string path, Transform at)
      where TObject : Object =>
      Object.Instantiate<TObject>(LoadResource<TObject>(path), at.position, at.rotation);

    public TObject InstantiateAt<TObject>(string path, Vector3 at, Quaternion rotation)
      where TObject : Object =>
      Object.Instantiate<TObject>(LoadResource<TObject>(path), at, rotation);

    public TResources LoadResource<TResources>(string path)
      where TResources : class =>
      Resources.Load(path) as TResources;
  }
}