﻿using System.Threading.Tasks;
using UnityEngine;

namespace Services.Assets
{
  public interface IAssetProvider : IService
  {
    Task<T> LoadAsync<T>(string address) where T : class;

    TObject Instantiate<TObject>(string path)
      where TObject : Object;

    TObject InstantiateAt<TObject>(string path, Transform at)
      where TObject : Object;

    TObject InstantiateAt<TObject>(string path, Vector3 at, Quaternion rotation)
      where TObject : Object;

    TResources LoadResource<TResources>(string path)
      where TResources : class;

    Task<GameObject> InstantiateAtAsync(string address, Vector3 at, Quaternion rotation);
    Task<GameObject> InstantiateAsync(string address);
    Task<GameObject> InstantiateAtAsync(string address, Transform at);
    void Initialize();
  }
}