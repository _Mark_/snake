﻿using Data;

namespace Services.PersistentProgressService
{
  public interface IPersistentProgressService : IService
  {
    PlayerProgress PlayerProgress { get; set; }
  }
}