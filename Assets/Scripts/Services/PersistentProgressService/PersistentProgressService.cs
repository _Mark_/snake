﻿using Data;

namespace Services.PersistentProgressService
{
  public class PersistentProgressService : IPersistentProgressService
  {
    public PlayerProgress PlayerProgress { get; set; }
  }
}