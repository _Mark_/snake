﻿using Data;

namespace Services.PersistentProgressService.SavedData
{
  public interface ISavedData
  {
    void SaveProgress(PlayerProgress playerProgress);
  }
}