﻿using Data;

namespace Services.PersistentProgressService.SavedData
{
  public interface ILoadedData
  {
    void LoadProgress(PlayerProgress playerProgress);
  }
}