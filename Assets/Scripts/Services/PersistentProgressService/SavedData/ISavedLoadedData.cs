﻿namespace Services.PersistentProgressService.SavedData
{
  public interface ISavedLoadedData : ISavedData, ILoadedData
  { }
}