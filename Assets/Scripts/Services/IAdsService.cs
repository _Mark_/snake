﻿using Services.Ads;

namespace Services
{
  public interface IAdsService : IService
  {
    Interstitial Interstitial { get; }
    Rewarded Rewarded { get; }
  }
}