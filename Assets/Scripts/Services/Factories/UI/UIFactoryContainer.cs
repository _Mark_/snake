using UnityEngine;

namespace Services.Factories.UI
{
  public class UIFactoryContainer
  {
    public Transform Root;

    public void Cleanup() => 
      Root = null;
  }
}