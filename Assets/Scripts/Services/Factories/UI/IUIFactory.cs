﻿using System.Threading.Tasks;
using GameMechanics.TimerForLevel;
using Services.Windows;

namespace Services.Factories.UI
{
  public interface IUIFactory : IService
  {
    void Initialize(IWindowsService windowsService);
    UIFactoryContainer Container { get; }
    void CreateRoot();
    void CreateEndLevelWindow(bool enableAd);
    void CreateDeathWindow();
    void CreateMenu();
    void CreateTimeEndWindow(TimerOfLevel timer);
    void CreateLevelListWindow();
  }
}