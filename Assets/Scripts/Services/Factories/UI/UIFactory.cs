﻿using System.Threading.Tasks;
using GameMechanics.TimerForLevel;
using Services.Assets;
using Services.Factories.Game;
using Services.LevelTransfer;
using Services.PersistentProgressService;
using Services.StaticData;
using Services.Windows;
using UI.Windows;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Services.Factories.UI
{
  public class UIFactory : IUIFactory
  {
    private const string _levelEnd = "Level end.";
    private const string _gameOver = "Game over.";
    private const string _menuTitle = "Menu.";
    private const string _timeIsOver = "Time is over.";

    private readonly IAssetProvider _assets;
    private readonly IPersistentProgressService _persistentProgress;
    private readonly GameFactoryContainer _gameFactoryContainer;
    private readonly ILevelTransferService _levelTransfer;
    private readonly IAdsService _adsService;
    private readonly IStaticDatasetService _staticData;
    private IWindowsService _windowsService;

    public void Initialize(IWindowsService windowsService) =>
      _windowsService = windowsService;

    public UIFactoryContainer Container { get; }

    public UIFactory(IAssetProvider assets, IPersistentProgressService persistentProgress,
      ILevelTransferService levelTransfer, GameFactoryContainer gameFactoryContainer, IAdsService adsService,
      IStaticDatasetService staticData)
    {
      _assets = assets;
      _persistentProgress = persistentProgress;
      _gameFactoryContainer = gameFactoryContainer;
      _adsService = adsService;
      _staticData = staticData;
      _levelTransfer = levelTransfer;
      Container = new UIFactoryContainer();
    }

    public async void CreateEndLevelWindow(bool enableAd)
    {
      GameObject backgroundWindow = InstantiateBackgroundWindow();
      GameObject endLevelWindow = await InstantiateEndLevelWindowAsync();

      InitializeEndLevelWindow();
      InitializeBackgroundWindow(backgroundWindow, _levelEnd);

      void InitializeEndLevelWindow()
      {
        endLevelWindow.GetComponent<EndLevelWindow>().Constructor(enableAd, _gameFactoryContainer, _levelTransfer,
          _adsService, backgroundWindow, _persistentProgress, _staticData.ForLevel(SceneManager.GetActiveScene().name));
      }
    }

    public async void CreateDeathWindow()
    {
      GameObject backgroundWindow = InstantiateBackgroundWindow();
      GameObject deathWindow = await InstantiateDeathWindowAsync();

      InitializeDeathWindow();
      InitializeBackgroundWindow(backgroundWindow, _gameOver);

      void InitializeDeathWindow() =>
        deathWindow.GetComponent<DeathWindow>().Constructor(_levelTransfer, _adsService, backgroundWindow);
    }

    public async void CreateMenu()
    {
      GameObject backgroundWindow = InstantiateBackgroundWindow();
      GameObject menuWindow = await InstantiateMenuWindowAsync();

      InitializeMenuWindow();
      InitializeBackgroundWindow(backgroundWindow, _menuTitle);

      void InitializeMenuWindow() =>
        menuWindow.GetComponent<MenuWindow>().Constructor(backgroundWindow, _levelTransfer, _windowsService,
          _persistentProgress, _staticData);
    }

    public async void CreateTimeEndWindow(TimerOfLevel timer)
    {
      GameObject backgroundWindow = InstantiateBackgroundWindow();
      GameObject timeEndWindow = await InstantiateTimeEndWindowAsync();

      InitializeTimeEndLevel();
      InitializeBackgroundWindow(backgroundWindow, _timeIsOver);

      void InitializeTimeEndLevel() =>
        timeEndWindow.GetComponent<TimeEndWindow>().Constructor(backgroundWindow, timer, _adsService, _levelTransfer,
          _persistentProgress, _staticData);
    }

    public async void CreateLevelListWindow()
    {
      GameObject backgroundWindow = InstantiateBackgroundWindow();
      GameObject levelListWindow = await InstantiateLevelListWindowAsync();
    }

    public void CreateRoot()
    {
      Container.Root = _assets.Instantiate<GameObject>(Const.ResourcesPaths.UI.Root).transform;
      RenameRoot();

      void RenameRoot() =>
        Container.Root.name = Container.Root.name.Replace("(Clone)", "");
    }

    private void InitializeBackgroundWindow(GameObject backgroundWindow, string title)
    {
      BackgroundWindow background = backgroundWindow.GetComponent<BackgroundWindow>();
      background.Construct(title);
    }

    private void SetRoot(GameObject obj) =>
      obj.transform.SetParent(Container.Root);

    private GameObject InstantiateBackgroundWindow()
    {
      GameObject backgroundWindow = _assets.Instantiate<GameObject>(Const.ResourcesPaths.UI.BackgroundWindow);
      SetRoot(backgroundWindow);
      return backgroundWindow;
    }

    private async Task<GameObject> InstantiateDeathWindowAsync()
    {
      GameObject deathWindow = await InstantiateAsync(Const.ResourcesPaths.UI.DeathWindow);
      SetRoot(deathWindow);
      return deathWindow;
    }

    private async Task<GameObject> InstantiateEndLevelWindowAsync()
    {
      GameObject endLevelWindow = await InstantiateAsync(Const.ResourcesPaths.UI.EndLevelWindow);
      SetRoot(endLevelWindow);
      return endLevelWindow;
    }

    private async Task<GameObject> InstantiateMenuWindowAsync()
    {
      GameObject menuWindow = await InstantiateAsync(Const.ResourcesPaths.UI.MenuWindow);
      SetRoot(menuWindow);
      return menuWindow;
    }

    private async Task<GameObject> InstantiateLevelListWindowAsync()
    {
      GameObject levelListWindow = await InstantiateAsync(Const.ResourcesPaths.UI.LevelListWindow);
      SetRoot(levelListWindow);
      return levelListWindow;
    }

    private async Task<GameObject> InstantiateTimeEndWindowAsync()
    {
      GameObject timeEndWindow = await InstantiateAsync(Const.ResourcesPaths.UI.TimeEndWindow);
      SetRoot(timeEndWindow);
      return timeEndWindow;
    }

    private async Task<GameObject> InstantiateAsync(string path) =>
      await _assets.InstantiateAsync(path);
  }
}