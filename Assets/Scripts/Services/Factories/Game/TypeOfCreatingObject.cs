namespace Services.Factories.Game
{
  public enum TypeOfCreatingObject
  {
    Apple,
    BadApple,
    Scope,
    Skull
  }
}