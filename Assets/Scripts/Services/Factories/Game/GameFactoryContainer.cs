﻿using GameMechanics.Snake.Movement;
using GameMechanics.Snake.TargetDesignator;
using UnityEngine;

namespace Services.Factories.Game
{
  public class GameFactoryContainer
  {
    public GameObject Head;
    public GameObject Joystick;
    public GameObject SnakeScreenData;

    public MovementTailChains MovementTailChainsPlayer =>
      Head.GetComponent<MovementTailChains>();

    public ControllerTargetDesignatorLine ControllerTargetDesignatorLine =>
      Head.GetComponentInChildren<ControllerTargetDesignatorLine>();

    public void CleanUp()
    {
      Head = null;
      Joystick = null;
      SnakeScreenData = null;
    }
  }
}