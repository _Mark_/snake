﻿using System.Linq;
using System.Threading.Tasks;
using GameMechanics;
using GameMechanics.EnemySnake;
using GameMechanics.TransferTrigger;
using Services.Assets;
using Services.Factories.Game.Builders;
using Services.Level;
using Services.PersistentProgressService;
using Services.StaticData;
using Services.Windows;
using StaticData;
using UnityEngine;

namespace Services.Factories.Game
{
  public class GameFactory : IGameFactory
  {
    private readonly IStaticDatasetService _staticData;

    public GameFactoryContainer Container { get; }
    public IAssetProvider AssetProvider { get; }
    public IWindowsService WindowsService { get; }
    public IPersistentProgressService ProgressService { get; }
    public ILevelService LevelService { get; }

    public GameFactory(
      GameFactoryContainer gameFactoryContainer,
      IAssetProvider assetProvider,
      IPersistentProgressService progressService,
      IStaticDatasetService staticData,
      ILevelService levelService,
      IWindowsService windowsService)
    {
      _staticData = staticData;

      Container = gameFactoryContainer;
      ProgressService = progressService;
      AssetProvider = assetProvider;
      WindowsService = windowsService;
      LevelService = levelService;
    }

    public async Task<GameObject> CreateAsync(TypeOfCreatingObject objectType, PositionData positionData)
    {
      GameObject returnObject = null;

      switch (objectType)
      {
        case TypeOfCreatingObject.Apple:
          returnObject = await AssetProvider.InstantiateAtAsync(Const.ResourcesPaths.Apple,
            positionData.Position.AsVector3(), Quaternion.identity);
          break;
        case TypeOfCreatingObject.BadApple:
          returnObject = await AssetProvider.InstantiateAtAsync(Const.ResourcesPaths.BadApple,
            positionData.Position.AsVector3(), Quaternion.identity);
          break;
        case TypeOfCreatingObject.Scope:
          returnObject = await AssetProvider.InstantiateAtAsync(Const.ResourcesPaths.Scope,
            positionData.Position.AsVector3(), Quaternion.identity);
          break;
        case TypeOfCreatingObject.Skull:
          returnObject = await AssetProvider.InstantiateAtAsync(Const.ResourcesPaths.Skull,
            positionData.Position.AsVector3(), Quaternion.identity);
          break;
      }

      return returnObject;
    }

    public async Task<GameObject> Create(IGameFactoryBuilderAsync builderAsync)
    {
      builderAsync.Construct(this, _staticData);
      return await builderAsync.Build();
    }

    public async Task<GameObject> CreateEnemySnakeSpawn(SpawnEnemySnakeDataset spawnData)
    {
      GameObject lookAtPositionController =
        await AssetProvider.InstantiateAsync(Const.ResourcesPaths.LookAtPositionController);

      InitializeSpawnEnemySnake();
      InitializeLookAtPositionController();

      lookAtPositionController.SetActive(false);

      return lookAtPositionController;

      void InitializeSpawnEnemySnake()
      {
        SpawnEnemySnake spawnEnemySnake = lookAtPositionController.GetComponent<SpawnEnemySnake>();
        spawnEnemySnake.LenghtTile = spawnData.LenghtTile;
        spawnEnemySnake.SpeedSnake = spawnData.SpeedSnake;
        spawnEnemySnake.Constructor(this);
      }

      void InitializeLookAtPositionController()
      {
        LookAtPositionController controller = lookAtPositionController.GetComponent<LookAtPositionController>();
        controller.CurrentTarget = spawnData.CurrentTarget;
        controller.DistanceToChangedTargetPoint = spawnData.DistanceToChangedTargetPoint;
        controller.Points = spawnData
          .Points
          .Select(point => point.AsVector3())
          .ToList();
      }
    }

    public async Task<GameObject> CreateEnemySnake(Vector3 at, Quaternion rotation, float speedSnake)
    {
      GameObject enemySnake = await AssetProvider.InstantiateAtAsync(Const.ResourcesPaths.EnemySnakeHead, at, rotation);

      SetSpeedSnake(speedSnake, enemySnake);
      SetMovementTailChainsPlayer(enemySnake);
      return enemySnake;
    }

    public async Task CreateLevelTransferTrigger(string sceneName)
    {
      (Vector3 position,
        Vector3 rotation,
        Vector3 colliderCenter,
        Vector3 colliderSize) = LevelTransferTriggerDataset();

      GameObject triggerObj = await AssetProvider.InstantiateAtAsync(Const.ResourcesPaths.LevelTransferTrigger,
        position, Quaternion.Euler(rotation));
      LevelTransferTrigger().Constructor(colliderCenter, colliderSize, LevelService);

      LevelTransferTriggerDataset LevelTransferTriggerDataset() =>
        _staticData.ForLevel(sceneName).LevelTransferTriggerDataset;

      LevelTransferTrigger LevelTransferTrigger() =>
        triggerObj.GetComponent<LevelTransferTrigger>();
    }

    private void SetSpeedSnake(float speedSnake, GameObject enemySnake) =>
      enemySnake.GetComponent<MoveEnemySnakeHead>().Speed = speedSnake;

    private void SetMovementTailChainsPlayer(GameObject enemySnake) =>
      enemySnake.GetComponentInChildren<BreakChainSnake>().MovementTailChainsPlayer =
        Container.MovementTailChainsPlayer;

    private async Task<GameObject> PrefabApple() =>
      await AssetProvider.LoadAsync<GameObject>(Const.ResourcesPaths.Apple);
  }
}