using System.Threading.Tasks;
using Services.StaticData;
using UnityEngine;

namespace Services.Factories.Game.Builders
{
  public interface IGameFactoryBuilderAsync
  {
    void Construct(IGameFactory gameFactory, IStaticDatasetService staticData);
    Task<GameObject> Build();
  }
}