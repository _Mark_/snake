using System.Collections.Generic;
using System.Threading.Tasks;
using GameMechanics.SpawnerCollectibles;
using Services.Assets;
using Services.StaticData;
using StaticData.CollectibleObjects;
using UnityEngine;

namespace Services.Factories.Game.Builders
{
  public class BuilderAsyncSpawnerCollectibleObjects : IGameFactoryBuilderAsync
  {
    private IAssetProvider _assetProvider;
    private readonly SpawnerCollectibleObjectsDataset _collectibleObjectData;

    public BuilderAsyncSpawnerCollectibleObjects(SpawnerCollectibleObjectsDataset collectibleObjectData)
    {
      _collectibleObjectData = collectibleObjectData;
    }

    public void Construct(IGameFactory gameFactory, IStaticDatasetService staticData)
    {
      _assetProvider = gameFactory.AssetProvider;
    }

    public async Task<GameObject> Build()
    {
      GameObject spawnerObj = await _assetProvider.InstantiateAsync(Const.ResourcesPaths.SpawnerOfObjectsForSnake);

      ControllerSpawnerCollectibleObjects controller = Controller(spawnerObj);
      SpawnerCollectibleObjects spawner = Spawner(spawnerObj);

      Controller dataController = _collectibleObjectData.Controller;
      Spawner dataSpawner = _collectibleObjectData.Spawner;

      ControllerInitialize(controller, spawner, dataController);
      SpawnerInitialize(spawner, controller, dataSpawner, spawnerObj);

      return spawnerObj;
    }

    private static ControllerSpawnerCollectibleObjects Controller(GameObject spawnerObj) =>
      spawnerObj.GetComponent<ControllerSpawnerCollectibleObjects>();

    private static SpawnerCollectibleObjects Spawner(GameObject spawnerObj) =>
      spawnerObj.GetComponent<SpawnerCollectibleObjects>();

    private static void ControllerInitialize(ControllerSpawnerCollectibleObjects controller,
      SpawnerCollectibleObjects spawner, Controller dataController) =>
      controller.Construct(spawner, dataController.MaxQuantityObjects);

    private void SpawnerInitialize(
      SpawnerCollectibleObjects spawner,
      ControllerSpawnerCollectibleObjects controller,
      Spawner dataSpawner,
      GameObject spawnerObj)
    {
      List<SpawnOption> spawnOptionsList = SpawnOptions(dataSpawner, spawnerObj);
      SpawnOption[] spawnOption = spawnOptionsList.ToArray();
      spawner.Constructor(controller, dataSpawner.FirstPoint.AsVector3(), dataSpawner.SecondPoint.AsVector3(),
        spawnOption);
    }

    private List<SpawnOption> SpawnOptions(Spawner dataSpawner, GameObject spawnerObj)
    {
      List<SpawnOption> spawns = new List<SpawnOption>();

      if (dataSpawner.Spawns.HasFlag(SpawnType.Apple))
        spawns.Add(spawnerObj.GetComponent<SpawnApple>());

      if (dataSpawner.Spawns.HasFlag(SpawnType.BadApple))
        spawns.Add(spawnerObj.GetComponent<SpawnBadApple>());

      if (dataSpawner.Spawns.HasFlag(SpawnType.Scope))
        spawns.Add(spawnerObj.GetComponent<SpawnScope>());

      if (dataSpawner.Spawns.HasFlag(SpawnType.Skull))
        spawns.Add(spawnerObj.GetComponent<SpawnSkull>());

      return spawns;
    }
  }
}