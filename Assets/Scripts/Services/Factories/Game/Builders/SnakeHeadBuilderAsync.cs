using System.Threading.Tasks;
using GameMechanics.InputJoystick;
using GameMechanics.Snake;
using GameMechanics.Snake.BiteSomething;
using GameMechanics.Snake.Movement;
using Services.Assets;
using Services.Level;
using Services.PersistentProgressService;
using Services.StaticData;
using UnityEngine;

namespace Services.Factories.Game.Builders
{
  public class SnakeHeadBuilderAsync : IGameFactoryBuilderAsync
  {
    private readonly Transform _at;
    private readonly InputDrag _inputDrag;
    private IAssetProvider _assetProvider;
    private GameFactoryContainer _container;
    private IPersistentProgressService _progressService;
    private ILevelService _levelService;
    private GameObject _head;

    public SnakeHeadBuilderAsync(Transform at, InputDrag inputDrag)
    {
      _at = at;
      _inputDrag = inputDrag;
    }

    public void Construct(IGameFactory gameFactory, IStaticDatasetService staticData)
    {
      _container = gameFactory.Container;
      _assetProvider = gameFactory.AssetProvider;
      _progressService = gameFactory.ProgressService;
      _levelService = gameFactory.LevelService;
    }

    public async Task<GameObject> Build()
    {
      _head = await _assetProvider.InstantiateAtAsync(Const.ResourcesPaths.SnakeHeadPath, _at);
      SetProgressServiceForChildren();
      SetReferenceOnAssetProvider();
      SetLevelServiceForChildren();
      PullHeadInContainer();
      SetInputDragForSnakeHead();
      return _head;
    }

    private void SetProgressServiceForChildren() =>
      _head.GetComponentInChildren<CollectorCoins>().Constructor(_progressService);

    private void SetReferenceOnAssetProvider() =>
      _head.GetComponentInChildren<SpawnerTailChains>().Construct(_assetProvider);

    private void SetLevelServiceForChildren() =>
      _head.GetComponentInChildren<SnakeDeath>().Constructor(_levelService);

    private void PullHeadInContainer() =>
      _container.Head = _head;

    private void SetInputDragForSnakeHead() =>
      _head.GetComponentInChildren<RotationHead>().InputService = _inputDrag;
  }
}