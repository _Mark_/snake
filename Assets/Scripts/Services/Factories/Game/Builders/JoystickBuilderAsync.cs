using System.Threading.Tasks;
using Services.Assets;
using Services.StaticData;
using UnityEngine;

namespace Services.Factories.Game.Builders
{
  public class JoystickBuilderAsync : IGameFactoryBuilderAsync
  {
    private GameFactoryContainer _container;
    private IAssetProvider _assetProvider;

    public void Construct(IGameFactory gameFactory, IStaticDatasetService staticData)
    {
      _container = gameFactory.Container;
      _assetProvider = gameFactory.AssetProvider;
    }

    public async Task<GameObject> Build()
    {
      GameObject joystick = await _assetProvider.InstantiateAsync(Const.ResourcesPaths.JoystickPathUI);
      _container.Joystick = joystick;
      return joystick;
    }
  }
}