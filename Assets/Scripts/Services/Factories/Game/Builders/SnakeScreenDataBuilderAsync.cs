using System.Threading.Tasks;
using GameMechanics.TimerForLevel;
using Services.Assets;
using Services.StaticData;
using Services.Windows;
using UI;
using UI.Windows;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Services.Factories.Game.Builders
{
  public class SnakeScreenDataBuilderAsync : IGameFactoryBuilderAsync
  {
    private IAssetProvider _assetProvider;
    private GameFactoryContainer _container;
    private IWindowsService _windowsService;
    private IStaticDatasetService _staticData;

    public void Construct(IGameFactory gameFactory, IStaticDatasetService staticData)
    {
      _assetProvider = gameFactory.AssetProvider;
      _container = gameFactory.Container;
      _windowsService = gameFactory.WindowsService;
      _staticData = staticData;
    }

    public async Task<GameObject> Build()
    {
      GameObject snakeScreenData = await InstantiateSnakeScreenData();
      PutInContainer(snakeScreenData);
      LinkCounterSetDependency(snakeScreenData);
      DamageScreenSetDependency(snakeScreenData);
      MenuButtonSetDependency(snakeScreenData);
      CallTimerEndWindowSetDependency(snakeScreenData);
      InitializeTimerOfLevel(snakeScreenData, TimeOfLevel());
      GameObject scopeTimerObj = ScopeTimerSetDependency(snakeScreenData).gameObject;
      scopeTimerObj.SetActive(false);

      return snakeScreenData;
    }

    private Task<GameObject> InstantiateSnakeScreenData() =>
      _assetProvider.InstantiateAsync(Const.ResourcesPaths.SnakeTailLenghtUI);

    private void PutInContainer(GameObject snakeScreenData) =>
      _container.SnakeScreenData = snakeScreenData;

    void LinkCounterSetDependency(GameObject snakeScreenData) =>
      snakeScreenData.GetComponentInChildren<LinkCounter>().Constructor(_container.MovementTailChainsPlayer);

    void DamageScreenSetDependency(GameObject snakeScreenData) =>
      snakeScreenData.GetComponentInChildren<DamageScreen>().Constructor(_container.MovementTailChainsPlayer);

    private void MenuButtonSetDependency(GameObject snakeScreenData) =>
      snakeScreenData.GetComponentInChildren<MenuButton>().Constructor(_windowsService);

    private void CallTimerEndWindowSetDependency(GameObject snakeScreenData) =>
      snakeScreenData.GetComponentInChildren<CallTimerEndWindow>().Construct(_windowsService);

    private static void InitializeTimerOfLevel(GameObject snakeScreenData, float amount) =>
      snakeScreenData.GetComponentInChildren<TimerOfLevel>().AddSeconds(amount);

    ScopeTimer ScopeTimerSetDependency(GameObject snakeScreenData)
    {
      ScopeTimer scopeTimer = snakeScreenData.GetComponentInChildren<ScopeTimer>();
      scopeTimer.Constructor(_container.ControllerTargetDesignatorLine);
      return scopeTimer;
    }

    private float TimeOfLevel() =>
      _staticData.ForLevel(SceneManager.GetActiveScene().name).TimeOfLevel.Value;
  }
}