using UnityEngine;

namespace Services.Factories.Game.Builders
{
  public interface IGameFactoryBuilder
  {
    void Construct(IGameFactory gameFactory);
    GameObject Build();
  }
}