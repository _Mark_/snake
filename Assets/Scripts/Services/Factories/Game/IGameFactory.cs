﻿using System.Threading.Tasks;
using Services.Assets;
using Services.Factories.Game.Builders;
using Services.Level;
using Services.PersistentProgressService;
using Services.Windows;
using StaticData;
using UnityEngine;

namespace Services.Factories.Game
{
  public interface IGameFactory : IService
  {
    GameFactoryContainer Container { get; }
    IAssetProvider AssetProvider { get; }
    IWindowsService WindowsService { get; }
    IPersistentProgressService ProgressService { get; }
    ILevelService LevelService { get; }
    Task<GameObject> Create(IGameFactoryBuilderAsync builderAsync);
    Task<GameObject> CreateEnemySnake(Vector3 at, Quaternion rotation, float speedSnake);
    Task<GameObject> CreateEnemySnakeSpawn(SpawnEnemySnakeDataset spawnData);
    Task CreateLevelTransferTrigger(string sceneName);
    Task<GameObject> CreateAsync(TypeOfCreatingObject type, PositionData applePosition);
  }
}