﻿namespace Services.LevelTransfer
{
  public interface ILevelTransferService : IService
  {
    void TransferToNextLevel(string activeScenePath);
    void TransferToLevel(string activeScenePath);
  }
}