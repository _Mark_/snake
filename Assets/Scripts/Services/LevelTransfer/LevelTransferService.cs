﻿using System.Collections.Generic;
using System.Linq;
using Infrastructure.StateMachine;
using Services.StaticData;

namespace Services.LevelTransfer
{
  public class LevelTransferService : ILevelTransferService
  {
    private readonly List<string> _levelList;
    private readonly GameStateMachine _stateMachine;

    public LevelTransferService(IStaticDatasetService staticDataset, GameStateMachine stateMachine)
    {
      _stateMachine = stateMachine;
      _levelList = staticDataset.ForLevelList().PathsScenes;
    }

    public void TransferToNextLevel(string activeScenePath)
    {
      string nextLevelPath = NextLevelPath(activeScenePath);

      if (nextLevelPath != default)
        _stateMachine.Enter<LoadLevelState, string>(nextLevelPath);
      else
        TransferToFirstLevel();
    }

    public void TransferToLevel(string activeScenePath) =>
      _stateMachine.Enter<LoadLevelState, string>(activeScenePath);

    private void TransferToFirstLevel() =>
      TransferToLevel(FirstLevelPath());

    private string NextLevelPath(string activeScenePath)
    {
      int currentLevelIndex = _levelList.IndexOf(activeScenePath);
      return _levelList.ElementAtOrDefault(currentLevelIndex + 1);
    }

    private string FirstLevelPath() =>
      _levelList[0];
  }
}