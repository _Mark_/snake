namespace Services.GameDataHandler
{
  public interface IGameDataHandlerService : IService
  {
    void EndLevel(string pathToScene);
  }
}