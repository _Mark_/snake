using System.Collections.Generic;
using System.Linq;
using Data;
using Services.PersistentProgressService;
using Services.StaticData;

namespace Services.GameDataHandler
{
  public class GameDataHandlerService : IGameDataHandlerService
  {
    private readonly IPersistentProgressService _persistentProgress;
    private readonly List<string> _levelList;

    private PlayerProgress PlayerProgress =>
      _persistentProgress.PlayerProgress;

    public GameDataHandlerService(IStaticDatasetService staticData, IPersistentProgressService persistentProgress)
    {
      _persistentProgress = persistentProgress;
      _levelList = staticData.ForLevelList().PathsScenes;
    }

    public void EndLevel(string pathToScene)
    {
      string nextLevelPath = NextLevelPathAfter(pathToScene);

      if (nextLevelPath != default)
        SetLastLevel(nextLevelPath);
      else
        SetFirstLevelAsLast();
    }

    private string NextLevelPathAfter(string pathToScene)
    {
      int currentLevelIndex = _levelList.IndexOf(pathToScene);
      return _levelList.ElementAtOrDefault(currentLevelIndex + 1);
    }

    private void SetLastLevel(string nextLevelPath) =>
      PlayerProgress.LastLevel.Path = nextLevelPath;

    private void SetFirstLevelAsLast()
    {
      string lastLevel = _levelList[0];
      PlayerProgress.LastLevel.Path = lastLevel;
    }
  }
}