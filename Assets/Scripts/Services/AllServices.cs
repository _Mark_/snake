﻿using System;
using System.Runtime.InteropServices;
using UnityEngine;

namespace Services
{
  public class AllServices
  {
    private static AllServices _instance;
    public static AllServices Container => _instance ?? (_instance = new AllServices());

    public AllServices Register<TService>(TService service)
      where TService : IService
    {
      Implementation<TService>.Instance = service;
      return this;
    }

    public TService RegisterService<TService>(TService service)
      where TService : IService =>
      Implementation<TService>.Instance = service;

    public TService Single<TService>()
      where TService : IService
    {
      TService service = Implementation<TService>.Instance;

      if (service == null)
        Debug.LogError($"No founded instance service: {typeof(TService)}");

      return service;
    }

    public void Dispose<TDisposable>()
      where TDisposable : class, IDisposable, IService
    {
      if (Implementation<TDisposable>.Dispose())
      {
        Implementation<TDisposable>.Instance.Dispose();
        Implementation<TDisposable>.Instance = null;
      }
    }

    private static class Implementation<TService>
      where TService : IService
    {
      private static int _references;
      private static TService _instance;

      public static TService Instance
      {
        get
        {
          _references++;
          return _instance;
        }
        set
        {
          _references = 0;
          _instance = value;
        }
      }

      public static bool Dispose()
      {
        _references--;
        return _references <= 0;
      }
    }
  }
}