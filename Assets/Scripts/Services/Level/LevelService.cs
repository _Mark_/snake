using Services.GameDataHandler;
using Services.Windows;

namespace Services.Level
{
  public class LevelService : ILevelService
  {
    private readonly IWindowsService _windowsService;
    private readonly IGameDataHandlerService _dataHandler;

    public LevelService(IWindowsService windowsService, IGameDataHandlerService dataHandler)
    {
      _windowsService = windowsService;
      _dataHandler = dataHandler;
    }

    public void EndLevel(string pathToScene)
    {
      _dataHandler.EndLevel(pathToScene);
      _windowsService.Open(WindowType.EndLevel);
    }

    public void DeathSnake(string pathToScene) =>
      _windowsService.Open(WindowType.Death);
  }
}