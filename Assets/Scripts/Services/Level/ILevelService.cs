namespace Services.Level
{
  public interface ILevelService : IService
  {
  void EndLevel(string pathToScene);
  void DeathSnake(string pathToScene);
  }
}