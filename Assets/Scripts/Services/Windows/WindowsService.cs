using GameMechanics.TimerForLevel;
using Services.Factories.Game;
using Services.Factories.UI;
using Services.PersistentProgressService;

namespace Services.Windows
{
  internal class WindowsService : IWindowsService
  {
    private readonly IUIFactory _uiFactory;
    private readonly GameFactoryContainer _gameContainer;
    private readonly IPersistentProgressService _progressService;

    public WindowsService(IUIFactory uiFactory, GameFactoryContainer gameContainer, IPersistentProgressService progressService)
    {
      _uiFactory = uiFactory;
      _gameContainer = gameContainer;
      _progressService = progressService;
    }

    public void Open(WindowType type)
    {
      switch (type)
      {
        case WindowType.EndLevel:
          OpenEndLevelWindow();
          break;
        case WindowType.Death:
          OpenDeathWindow();
          break;
        case WindowType.Menu:
          OpenMenuWindow();
          break;
        case WindowType.LevelListWindow:
          OpenLevelListWindow();
          break;
        case WindowType.TimeEndWindow:
          OpenTimeEndWindow();
          break;
      }
    }

    private void OpenEndLevelWindow()
    {
      DisableJoystickAndSnakeScreenData();
      _uiFactory.CreateEndLevelWindow(_progressService.PlayerProgress.LevelNumber.Parity);
    }

    private void OpenDeathWindow()
    {
      DisableJoystickAndSnakeScreenData();
      _uiFactory.CreateDeathWindow();
    }

    private void OpenMenuWindow() =>
      _uiFactory.CreateMenu();

    private void OpenLevelListWindow() =>
      _uiFactory.CreateLevelListWindow();

    private void OpenTimeEndWindow() =>
      _uiFactory.CreateTimeEndWindow(TimerOfLevel());

    private void DisableJoystickAndSnakeScreenData()
    {
      _gameContainer.Joystick.SetActive(false);
      _gameContainer.SnakeScreenData.SetActive(false);
    }

    private TimerOfLevel TimerOfLevel() =>
      _gameContainer.SnakeScreenData.GetComponentInChildren<TimerOfLevel>();
  }
}