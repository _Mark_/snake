namespace Services.Windows
{
  public enum WindowType
  {
    EndLevel,
    Death,
    Menu,
    LevelListWindow,
    TimeEndWindow
  }
}