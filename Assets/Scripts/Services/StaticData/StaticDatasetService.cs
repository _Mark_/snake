﻿using System.Collections.Generic;
using System.Linq;
using StaticData;
using UnityEngine;

namespace Services.StaticData
{
  public class StaticDatasetService : IStaticDatasetService
  {
    private Dictionary<string, LevelStaticData> _levels;
    private LevelList _levelList;

    public void Load()
    {
      _levels = Resources
        .LoadAll<LevelStaticData>("StaticData/Level")
        .ToDictionary(data => data.LevelKey, data => data);

      _levelList = Resources
        .Load<LevelList>("StaticData/LevelList");
    }

    public LevelStaticData ForLevel(string level)
    {
      if (_levels.TryGetValue(level, out LevelStaticData leveData))
        return leveData;

      Debug.LogError($"Key {level} is not founded.");
      return null;
    }

    public LevelList ForLevelList() =>
      _levelList;
  }
}