﻿using StaticData;

namespace Services.StaticData
{
  public interface IStaticDatasetService : IService
  {
    void Load();
    LevelStaticData ForLevel(string level);
    LevelList ForLevelList();
  }
}