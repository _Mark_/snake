using System;
using GoogleMobileAds.Api;
using UnityEngine;

namespace Services.Ads
{
  public class Rewarded
  {
    private const string _adUnitId = "ca-app-pub-5386101324592499/7554651613";
    private const string TEST_adUnitId = "ca-app-pub-3940256099942544/5224354917";

    private RewardedAd _rewardedAd;
    private bool _testMode;

    public event Action AdLoaded;
    public event Action AdFailedToLoad;
    public event Action AdFailedToShow;
    public event Action<string, double> UserEarnedReward;
    public event Action AdClosed;

    public bool IsLoadedAd =>
      _rewardedAd.IsLoaded();

    public Rewarded(bool test) =>
      Initialize(test);

    private void Initialize(bool test)
    {
      _testMode = test;
      LoadAd();
    }

    public void Show()
    {
      if (_rewardedAd.IsLoaded())
        _rewardedAd.Show();
    }
    
    public void LoadAd()
    {
      InitializeRewardedAdAd(_testMode);
      SubscribeOnRewardedAdEvents();
      AdRequest request = new AdRequest.Builder().Build();
      _rewardedAd.LoadAd(request);
    }

    private void InitializeRewardedAdAd(bool test) =>
      _rewardedAd = test
        ? new RewardedAd(TEST_adUnitId)
        : new RewardedAd(_adUnitId);

    private void SubscribeOnRewardedAdEvents()
    {
      _rewardedAd.OnAdLoaded += OnAdLoaded;
      _rewardedAd.OnAdFailedToLoad += OnAdFailedToLoad;
      _rewardedAd.OnAdFailedToShow += OnAdFailedToShow;
      _rewardedAd.OnUserEarnedReward += OnUserEarnedReward;
      _rewardedAd.OnAdClosed += OnAdClosed;
    }

    private void OnAdLoaded(object sender, EventArgs e)
    {
      AdLoaded?.Invoke();
    }

    private void OnAdFailedToLoad(object sender, AdErrorEventArgs e)
    {
      AdFailedToLoad?.Invoke();
    }

    private void OnAdFailedToShow(object sender, AdErrorEventArgs e)
    {
      AdFailedToShow?.Invoke();
    }

    private void OnUserEarnedReward(object sender, Reward e)
    {
      UserEarnedReward?.Invoke(e.Type, e.Amount);
    }

    private void OnAdClosed(object sender, EventArgs e)
    {
      LoadAd();
      AdClosed?.Invoke();
    }
  }
}