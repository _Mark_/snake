using System;
using GoogleMobileAds.Api;

namespace Services.Ads
{
  public class Interstitial
  {
    private const string _adUnitId = "ca-app-pub-5386101324592499/6842660587";
    private const string TESTadUnitId = "ca-app-pub-3940256099942544/1033173712";

    private InterstitialAd _interstitialAd;

    public Interstitial(bool test) =>
      Initialize(test);

    public event Action AdClosed;

    public bool IsLoadedAd =>
      _interstitialAd.IsLoaded();

    public void Show()
    {
      if (_interstitialAd.IsLoaded())
        _interstitialAd.Show();
    }

    public void LoadAd()
    {
      AdRequest request = new AdRequest.Builder().Build();
      _interstitialAd.LoadAd(request);
    }

    private void Initialize(bool test)
    {
      InitializeInterstitialAd(test);
      SubscribeOnInterstitialAdEvents();
    }

    private void InitializeInterstitialAd(bool test) =>
      _interstitialAd = test
        ? new InterstitialAd(TESTadUnitId)
        : new InterstitialAd(_adUnitId);

    private void SubscribeOnInterstitialAdEvents() =>
      _interstitialAd.OnAdClosed += OnAdClosed;

    private void OnAdClosed(object sender, EventArgs e)
    {
      LoadAd();
      AdClosed?.Invoke();
    }
  }
}