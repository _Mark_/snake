﻿using GoogleMobileAds.Api;

namespace Services.Ads
{
  public class AdsService : IAdsService
  {
    public Interstitial Interstitial { get; private set; }
    public Rewarded Rewarded { get; private set; }

    public AdsService(bool test)
    {
      MobileAdsInitialize();
      Interstitial = new Interstitial(test);
      Rewarded = new Rewarded(test);
    }

    private static void MobileAdsInitialize() =>
      MobileAds.Initialize(initStatus => { });
  }
}