﻿using System.Collections.Generic;
using System.IO;
using Data;
using Services.PersistentProgressService;
using Services.PersistentProgressService.SavedData;
using UnityEngine;

namespace Services.SaveLoadService
{
  public class SaveLoadDataService : ISaveLoadDataService
  {
    private readonly IPersistentProgressService _progressService;
    private readonly List<ISavedData> _savings;
    private readonly List<ILoadedData> _loadings;

    public SaveLoadDataService(IPersistentProgressService progressService)
    {
      _progressService = progressService;
      _savings = new List<ISavedData>();
      _loadings = new List<ILoadedData>();
    }

    public void SaveProgress() =>
      SaveLoader.SavingInsideJsonFile(_progressService.PlayerProgress);

    public PlayerProgress LoadProgress() =>
      _progressService.PlayerProgress = SaveLoader.LoadingFromJsonFile() ?? new PlayerProgress();

    public void SavePlayerProgress()
    {
      foreach (ISavedData saving in _savings)
        saving.SaveProgress(_progressService.PlayerProgress);
    }

    public void LoadPlayerProgress()
    {
      foreach (ILoadedData loading in _loadings)
        loading.LoadProgress(_progressService.PlayerProgress);
    }

    public void AddListenerSave(ISavedData saved) =>
      _savings.Add(saved);

    public void AddListenerLoad(ILoadedData loaded) =>
      _loadings.Add(loaded);

    public void AddListenerSaveLoad(ISavedLoadedData savedLoader)
    {
      AddListenerLoad(savedLoader);
      AddListenerSave(savedLoader);
    }

    public void Cleanup()
    {
      _savings.Clear();
      _loadings.Clear();
    }


    /*------------------------------------------------------------------------------------------------*/


    private static class SaveLoader
    {
      private const string _savingDataKey = "SavsingData";
      private const string _progressFileName = "PlayerProgress.json";

      public static void SavingInsideJsonFile(PlayerProgress playerProgress)
      {
        string json = JsonUtility.ToJson(playerProgress, true);
        File.WriteAllText(PathToProgress(), json);
      }

      public static PlayerProgress LoadingFromJsonFile()
      {
        if (File.Exists(PathToProgress()))
        {
          string json = File.ReadAllText(PathToProgress());
          return JsonUtility.FromJson<PlayerProgress>(json);
        }

        return null;
      }

      public static void SaveInsidePlayerPrefs(PlayerProgress playerProgress)
      {
        string jsonData = JsonUtility.ToJson(playerProgress);
        PlayerPrefs.SetString(_savingDataKey, jsonData);
      }

      public static PlayerProgress LoadDataFromPlayerPrefs()
      {
        if (PlayerPrefs.HasKey(_savingDataKey))
        {
          string jsonData = PlayerPrefs.GetString(_savingDataKey);
          return JsonUtility.FromJson<PlayerProgress>(jsonData);
        }

        return null;
      }

      private static string PathToProgress() =>
        Path.Combine(Application.persistentDataPath, _progressFileName);
    }
  }
}