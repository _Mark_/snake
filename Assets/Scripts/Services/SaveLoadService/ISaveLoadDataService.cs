﻿using Data;
using Services.PersistentProgressService.SavedData;

namespace Services.SaveLoadService
{
  public interface ISaveLoadDataService : IService
  {
    void SaveProgress();
    PlayerProgress LoadProgress();
    void SavePlayerProgress();
    void LoadPlayerProgress();
    void AddListenerSave(ISavedData saved);
    void AddListenerLoad(ILoadedData loaded);
    void AddListenerSaveLoad(ISavedLoadedData savedLoader);
    void Cleanup();
  }
}