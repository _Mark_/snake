﻿using UnityEngine;

namespace Infrastructure
{
  public class GameBootstrapper : MonoBehaviour, ICoroutineRunner
  {
    public string nameFirsScene;
    
    private Game _game;

    private void Start()
    {
      DontDestroyOnLoad(this);
      _game = new Game(this, nameFirsScene);
      Screen.sleepTimeout = SleepTimeout.NeverSleep;
    }
  }
}