﻿using Data;
using Services.SaveLoadService;
using Services.StaticData;

namespace Infrastructure.StateMachine
{
  public class LoadPlayerProgressState : IState
  {
    private readonly GameStateMachine _stateMachine;
    private readonly ISaveLoadDataService _saveLoadService;
    private string _nameFirsScene;
    private readonly IStaticDatasetService _staticData;

    public LoadPlayerProgressState(GameStateMachine stateMachine, string nameFirsScene,
      ISaveLoadDataService saveLoadService, IStaticDatasetService staticData)
    {
      _stateMachine = stateMachine;
      _saveLoadService = saveLoadService;
      _staticData = staticData;
      _nameFirsScene = nameFirsScene;
    }

    public void Enter()
    {
      PlayerProgress playerProgress = _saveLoadService.LoadProgress();
      LoadFirstScene(playerProgress);
    }

    public void Exit() { }

    private void LoadFirstScene(PlayerProgress playerProgress)
    {
      if (string.IsNullOrEmpty(_nameFirsScene))
        _nameFirsScene = playerProgress.LastLevel.Path;

      if (!LastLevelExistInLevelList(playerProgress))
        _nameFirsScene = null;
      
      if (string.IsNullOrEmpty(_nameFirsScene))
        _nameFirsScene = _staticData.ForLevelList().PathsScenes[0];
      
      _stateMachine.Enter<LoadLevelState, string>(_nameFirsScene);
    }

    private bool LastLevelExistInLevelList(PlayerProgress playerProgress) =>
      _staticData.ForLevelList().PathsScenes.Contains(playerProgress.LastLevel.Path);
  }
}