﻿using Services;
using Services.Assets;
using Services.Factories.Game;
using Services.Factories.UI;
using Services.LoadScene;
using Services.SaveLoadService;
using Services.StaticData;

namespace Infrastructure.StateMachine
{
  public class GameStateMachine
  {
    private IExitState _activeState;

    private readonly StateContainer<IExitState> _states;

    public GameStateMachine(string nameFirsScene, ISceneLoader sceneLoader, AllServices allServices)
    {
      _states = new StateContainer<IExitState>();
      _states
        .Add(new BootstrapperState(
          this,
          sceneLoader,
          allServices))
        .Add(new LoadPlayerProgressState(
          this,
          nameFirsScene,
          allServices.Single<ISaveLoadDataService>(),
          allServices.Single<IStaticDatasetService>()))
        .Add(new LoadLevelState
        (
          this,
          sceneLoader,
          allServices.Single<IGameFactory>(),
          allServices.Single<ISaveLoadDataService>(),
          allServices.Single<IUIFactory>()))
        .Add(new LoadResourcesState(
          this,
          allServices.Single<IGameFactory>(),
          allServices.Single<ISaveLoadDataService>(),
          allServices.Single<IStaticDatasetService>(),
          allServices.Single<IUIFactory>(),
          allServices.Single<IAdsService>(),
          allServices.Single<IAssetProvider>()))
        .Add(new GameLoopState(this));
    }

    public void Enter<TState>()
      where TState : class, IState
    {
      TState state = ChangeState<TState>();
      state.Enter();
    }

    public void Enter<TState, TPayload>(TPayload payload)
      where TState : class, IPayLoadState<TPayload>
    {
      TState state = ChangeState<TState>();
      state.Enter(payload);
    }


    private TState ChangeState<TState>()
      where TState : class, IExitState
    {
      _activeState?.Exit();
      TState state = GetState<TState>();
      _activeState = state;
      return state;
    }

    private TState GetState<TState>()
      where TState : class, IExitState =>
      _states.Get<TState>();
  }
}