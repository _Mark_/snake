﻿using System;
using System.Collections.Generic;

namespace Infrastructure.StateMachine
{
  public class StateContainer<TState>
  {
    private readonly Dictionary<Type, TState> _states;

    public StateContainer() =>
      _states = new Dictionary<Type, TState>();

    public StateContainer<TState> Add<TStateKey>(TStateKey state)
      where TStateKey : TState
    {
      _states[typeof(TStateKey)] = state;
      return this;
    }

    public TStateKey Get<TStateKey>()
      where TStateKey : class, TState =>
      _states[typeof(TStateKey)] as TStateKey;

  }
}