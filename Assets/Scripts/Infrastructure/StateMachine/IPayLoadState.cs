﻿namespace Infrastructure.StateMachine
{
  public interface IPayLoadState<in TParameter> : IExitState
  {
    void Enter(TParameter nameScene);
  }
}