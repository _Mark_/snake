﻿using Services.Factories.Game;
using Services.Factories.UI;
using Services.LoadScene;
using Services.SaveLoadService;

namespace Infrastructure.StateMachine
{
  public class LoadLevelState : IPayLoadState<string>
  {
    private readonly GameStateMachine _gameStateMachine;
    private readonly ISceneLoader _sceneLoader;
    private readonly IGameFactory _factory;
    private readonly ISaveLoadDataService _saveLoadData;
    private readonly IUIFactory _uiFactory;

    public LoadLevelState(
      GameStateMachine gameStateMachine,
      ISceneLoader sceneLoader,
      IGameFactory factory,
      ISaveLoadDataService saveLoadData,
      IUIFactory uiFactory)
    {
      _gameStateMachine = gameStateMachine;
      _sceneLoader = sceneLoader;
      _factory = factory;
      _saveLoadData = saveLoadData;
      _uiFactory = uiFactory;
    }

    public void Enter(string nameScene)
    {
      _saveLoadData.SaveProgress();
      _saveLoadData.Cleanup();
      _uiFactory.Container.Cleanup();
      _factory.Container.CleanUp();
      _sceneLoader.Load(nameScene, SceneLoaded);
    }

    public void Exit() { }

    private void SceneLoaded() =>
      _gameStateMachine.Enter<LoadResourcesState>();
  }
}