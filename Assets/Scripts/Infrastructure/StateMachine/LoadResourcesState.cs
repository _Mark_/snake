﻿using System.Threading.Tasks;
using GameMechanics.EnemySnake;
using GameMechanics.InputJoystick;
using Services;
using Services.Assets;
using Services.Factories.Game;
using Services.Factories.Game.Builders;
using Services.Factories.UI;
using Services.SaveLoadService;
using Services.StaticData;
using StaticData;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Infrastructure.StateMachine
{
  public class LoadResourcesState : IState
  {
    private readonly GameStateMachine _gameStateMachine;
    private readonly IGameFactory _gameFactory;
    private readonly ISaveLoadDataService _saveLoadDataService;
    private readonly IStaticDatasetService _staticData;
    private readonly IUIFactory _uiFactory;
    private readonly IAdsService _adsService;
    private readonly IAssetProvider _assetProvider;

    public LoadResourcesState(
      GameStateMachine gameStateMachine,
      IGameFactory gameFactory,
      ISaveLoadDataService saveLoadDataService,
      IStaticDatasetService staticData,
      IUIFactory uiFactory,
      IAdsService adsService,
      IAssetProvider assetProvider)
    {
      _gameStateMachine = gameStateMachine;
      _gameFactory = gameFactory;
      _saveLoadDataService = saveLoadDataService;
      _staticData = staticData;
      _uiFactory = uiFactory;
      _adsService = adsService;
      _assetProvider = assetProvider;
    }

    public async void Enter()
    {
      await InitializeWorld();
      InitializeServices();
      _gameStateMachine.Enter<GameLoopState>();
    }

    public void Exit() { }

    private async Task InitializeWorld()
    {
      _uiFactory.CreateRoot();
      LevelStaticData levelStaticData = LevelStaticData();
      InputDrag inputDrag = await InitializeJoystick();
      await InitializeSnakeHead(inputDrag);
      await InitializeSnakeScreenData();
      await InitializeEnemySnakes(levelStaticData);
      await InitializeLevelTransferTrigger();
      await InitializeSpawnerCollectibleObjects(levelStaticData);
      InitializeCollectiblesAsync(levelStaticData);
      DestroyAllMarkers();
      LoadPlayerProgress();
    }

    private LevelStaticData LevelStaticData() =>
      _staticData.ForLevel(SceneManager.GetActiveScene().name);

    private async Task<InputDrag> InitializeJoystick()
    {
      GameObject joystick = await _gameFactory.Create(new JoystickBuilderAsync());
      SetParentRootUI(joystick);
      return joystick.GetComponentInChildren<InputDrag>();
    }

    private async Task InitializeSnakeHead(InputDrag inputDrag)
    {
      // TODO: expansive operation "find".
      Transform initialPoint = GameObject.Find(Const.Tag.InitialPoint).transform;
      await _gameFactory.Create(new SnakeHeadBuilderAsync(initialPoint, inputDrag));
    }

    private async Task InitializeSnakeScreenData()
    {
      GameObject snakeScreenData = await _gameFactory.Create(new SnakeScreenDataBuilderAsync());
      SetParentRootUI(snakeScreenData);
    }

    private async Task InitializeEnemySnakes(LevelStaticData levelData)
    {
      foreach (SpawnEnemySnakeDataset spawnData in levelData.SpawnEnemySnakeDataset)
      {
        GameObject snakeSpawnObj = await _gameFactory.CreateEnemySnakeSpawn(spawnData);
        SpawnEnemySnake spawn = EnemySnakeSpawn(snakeSpawnObj);
        GameObject snakeHead = await spawn.Spawn();

        SpawnerEnemyTailChains spawnerEnemyTailChains = FactoryTailEnemyChains(snakeHead);

        InitializeLookAtPositionController(snakeSpawnObj, snakeHead);
        spawnerEnemyTailChains.Constructor(_assetProvider);
        await CreateTailEnemySnake(spawn, spawnerEnemyTailChains);

        snakeSpawnObj.SetActive(true);
      }

      SpawnEnemySnake EnemySnakeSpawn(GameObject snakeSpawnObj) =>
        snakeSpawnObj.GetComponent<SpawnEnemySnake>();

      SpawnerEnemyTailChains FactoryTailEnemyChains(GameObject snake) =>
        snake.GetComponentInChildren<SpawnerEnemyTailChains>();

      void InitializeLookAtPositionController(GameObject snakeSpawn, GameObject snakeHead)
      {
        LookAtPositionController controller = snakeSpawn.GetComponent<LookAtPositionController>();
        controller.Executor = snakeHead.transform;
        controller.LookAtPosition = snakeHead.GetComponent<LookAtPosition>();
      }

      async Task CreateTailEnemySnake(SpawnEnemySnake spawn, SpawnerEnemyTailChains factoryTailEnemyChains)
      {
        for (int i = 0; i < spawn.LenghtTile; i++)
          await factoryTailEnemyChains.CreateChainAsync();
      }
    }

    private async Task InitializeLevelTransferTrigger() =>
      await _gameFactory.CreateLevelTransferTrigger(SceneManager.GetActiveScene().name);

    private async Task InitializeSpawnerCollectibleObjects(LevelStaticData levelStaticData)
    {
      foreach (StaticData.CollectibleObjects.SpawnerCollectibleObjectsDataset collectibleObjectData in levelStaticData
        .SpawnerCollectibleObjects)
        await _gameFactory.Create(new BuilderAsyncSpawnerCollectibleObjects(collectibleObjectData));
    }

    private async void InitializeCollectiblesAsync(LevelStaticData levelStaticData)
    {
      foreach (PositionData applePosition in levelStaticData.ApplePositions)
        await _gameFactory.CreateAsync(TypeOfCreatingObject.Apple, applePosition);

      foreach (PositionData badApplePosition in levelStaticData.BadApplePositions)
        await _gameFactory.CreateAsync(TypeOfCreatingObject.BadApple, badApplePosition);

      foreach (PositionData scopePositions in levelStaticData.ScopePositions)
        await _gameFactory.CreateAsync(TypeOfCreatingObject.Scope, scopePositions);

      foreach (PositionData skullPositions in levelStaticData.SkullPositions)
        await _gameFactory.CreateAsync(TypeOfCreatingObject.Skull, skullPositions);
    }

    private void InitializeServices()
    {
      _adsService.Interstitial.LoadAd();
      _adsService.Rewarded.LoadAd();
    }

    private void DestroyAllMarkers()
    {
      foreach (GameObject marker in GameObject.FindGameObjectsWithTag(Const.Tag.DevelopMarker))
        Object.Destroy(marker);
    }

    private void LoadPlayerProgress() =>
      _saveLoadDataService.LoadPlayerProgress();

    private static void SetParentRootUI(GameObject uiElement)
    {
      GameObject uiRoot = GameObject.Find(Const.Scene.Hierarchy.UIRoot);
      uiElement.transform.SetParent(uiRoot.transform);
    }
  }
}