﻿using Services;
using Services.Ads;
using Services.Assets;
using Services.Factories.Game;
using Services.Factories.UI;
using Services.GameDataHandler;
using Services.Level;
using Services.LevelTransfer;
using Services.LoadScene;
using Services.PersistentProgressService;
using Services.SaveLoadService;
using Services.StaticData;
using Services.Windows;

namespace Infrastructure.StateMachine
{
  public class BootstrapperState : IState
  {
    private const string _nameInitialScene = "Initial";
    private const bool _adTestMode = false;

    private readonly GameStateMachine _gameStateMachine;
    private readonly ISceneLoader _sceneLoader;
    private readonly AllServices _allServices;

    public BootstrapperState(GameStateMachine gameStateMachine, ISceneLoader sceneLoader, AllServices allServices)
    {
      _allServices = allServices;
      _gameStateMachine = gameStateMachine;
      _sceneLoader = sceneLoader;

      RegisterService();
    }


    public void Enter()
    {
      InitializeServices();
      _sceneLoader.Load(_nameInitialScene, InitialSceneLoaded);
    }

    public void Exit() { }

    private void InitialSceneLoaded() =>
      _gameStateMachine.Enter<LoadPlayerProgressState>();

    private void RegisterService()
    {
      _allServices
        .RegisterService<IStaticDatasetService>(new StaticDatasetService())
        .Load();

      RegisterAdsService();

      RegisterAssetProviderService();

      _allServices
        .Register<ILevelTransferService>(new LevelTransferService(
          _allServices.Single<IStaticDatasetService>(),
          _gameStateMachine
        ))
        .Register<IPersistentProgressService>(new PersistentProgressService())
        .Register<IGameDataHandlerService>(new GameDataHandlerService(
          _allServices.Single<IStaticDatasetService>(),
          _allServices.Single<IPersistentProgressService>()
        ))
        .Register<ISaveLoadDataService>(new SaveLoadDataService(
          _allServices.Single<IPersistentProgressService>()
        ));

      GameFactoryContainer gameFactoryContainer = new GameFactoryContainer();

      _allServices
        .Register<IUIFactory>(new UIFactory(
          _allServices.Single<IAssetProvider>(),
          _allServices.Single<IPersistentProgressService>(),
          _allServices.Single<ILevelTransferService>(),
          gameFactoryContainer,
          _allServices.Single<IAdsService>(),
          _allServices.Single<IStaticDatasetService>()
        ))
        .Register<IWindowsService>(new WindowsService(
          _allServices.Single<IUIFactory>(),
          gameFactoryContainer,
          _allServices.Single<IPersistentProgressService>()
        ))
        .Register<ILevelService>(new LevelService(
          _allServices.Single<IWindowsService>(),
          _allServices.Single<IGameDataHandlerService>()
        ))
        .Register<IGameFactory>(new GameFactory(
          gameFactoryContainer,
          _allServices.Single<IAssetProvider>(),
          _allServices.Single<IPersistentProgressService>(),
          _allServices.Single<IStaticDatasetService>(),
          _allServices.Single<ILevelService>(),
          _allServices.Single<IWindowsService>()
        ));
    }

    private void RegisterAssetProviderService() =>
      _allServices
        .RegisterService<IAssetProvider>(new AssetProvider())
        .Initialize();

    private void InitializeServices() =>
      _allServices.Single<IUIFactory>()
        .Initialize(_allServices.Single<IWindowsService>());

    private void RegisterAdsService() =>
      _allServices.RegisterService<IAdsService>(new AdsService(_adTestMode));
  }
}