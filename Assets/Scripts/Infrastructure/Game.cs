﻿using Infrastructure.StateMachine;
using Services;
using Services.LoadScene;

namespace Infrastructure
{
  public class Game
  {
    private readonly GameStateMachine _gameStateMachine;

    public Game(ICoroutineRunner coroutineRunner, string nameFirsScene)
    {
      _gameStateMachine = new GameStateMachine(nameFirsScene, new SceneLoader(coroutineRunner),
        AllServices.Container);
      _gameStateMachine.Enter<BootstrapperState>();
    }
  }
}