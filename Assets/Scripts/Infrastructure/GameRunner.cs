﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Infrastructure
{
  public class GameRunner : MonoBehaviour
  {
    private const string _bootstrapperName = "GameBootstrapper";
    private const string _initialSceneName = "Initial";

    public GameBootstrapper Bootstrapper;

    private void Awake()
    {
      GameBootstrapper bootstrapper = FindObjectOfType<GameBootstrapper>();

      if (bootstrapper == null)
      {
        bootstrapper = Instantiate(Bootstrapper);
        bootstrapper.name = _bootstrapperName;

        if (SceneManager.GetActiveScene().name != _initialSceneName)
            bootstrapper.nameFirsScene = SceneManager.GetActiveScene().name;
      }

      Destroy(gameObject);
    }
  }
}