﻿using GameMechanics.Snake.Movement;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
  public class DamageScreen : MonoBehaviour
  {
    public Image Image;
    public GameObject Screen;
    public MovementTailChains MovementTailChains;
    public float ScreenTime;

    private Color _startColor;
    private Color _transparent;
    private float _timer;

    public void Constructor(MovementTailChains movementTailChains)
    {
      MovementTailChains = movementTailChains;
      MovementTailChains.ChainRemoved += ChainRemoved;
      Screen.SetActive(false);
    }

    private void Awake()
    {
      _startColor = Image.color;
      _transparent = new Color(_startColor.r, _startColor.g, _startColor.b, 0);
    }

    private void Update()
    {
      _timer += Time.deltaTime;
      
      if (_timer >= ScreenTime)
        Screen.SetActive(false);

      Image.color = Color.Lerp(_startColor, _transparent, _timer / ScreenTime);
    }

    private void ChainRemoved()
    {
      _timer = 0;
      Image.color = _startColor;
      Screen.SetActive(true);
    }

    private void OnDestroy() =>
      MovementTailChains.ChainRemoved -= ChainRemoved;
  }
}