﻿using GameMechanics.Snake.TargetDesignator;
using TMPro;
using UnityEngine;

namespace UI
{
  public class ScopeTimer : MonoBehaviour
  {
    public TextMeshProUGUI CountChainTail;
    public ControllerTargetDesignatorLine Controller;

    public void Constructor(ControllerTargetDesignatorLine controller)
    {
      Controller = controller;

      Controller.TimerEnabled += TimerEnabled;
      Controller.TimerDisabled += TimerDisabled;
      Controller.TimerUpdate += TimerUpdate;
    }

    private void TimerEnabled() =>
      gameObject.SetActive(true);

    private void TimerDisabled() =>
      gameObject.SetActive(false);

    private void TimerUpdate() =>
      CountChainTail.text = Controller.RemainingTime.ToString("00");

    private void OnDestroy()
    {
      Controller.TimerEnabled -= TimerEnabled;
      Controller.TimerDisabled -= TimerDisabled;
      Controller.TimerUpdate -= TimerUpdate;
    }
  }
}