using System.Collections;
using GameMechanics.TimerForLevel;
using Services;
using Services.LevelTransfer;
using Services.PersistentProgressService;
using Services.StaticData;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace UI.Windows
{
  public class TimeEndWindow : Window
  {
    [SerializeField] private float _durationMessageAdIsNotReady = 5f;
    [SerializeField] private float _durationEnableRestartButton = 5f;
    [SerializeField] private TextMeshProUGUI _coins = default;
    [SerializeField] private TextMeshProUGUI _numberLevel = default;
    [SerializeField] private GameObject _adIsNotReady = default;
    [SerializeField] private Button _restart = default;
    [SerializeField] private Button _showRewardAd = default;

    private GameObject _background;
    private IAdsService _adsService;
    private ILevelTransferService _levelTransfer;
    private IPersistentProgressService _persistentProgress;
    private IStaticDatasetService _staticData;
    private TimerOfLevel _timer;

    public void Constructor(GameObject background, TimerOfLevel timer, IAdsService adsService,
      ILevelTransferService levelTransfer,
      IPersistentProgressService persistentProgress, IStaticDatasetService staticData)
    {
      _background = background;
      _adsService = adsService;
      _levelTransfer = levelTransfer;
      _persistentProgress = persistentProgress;
      _staticData = staticData;
      _timer = timer;
    }

    protected override void Initialize()
    {
      RefreshCoins();
      RefreshLevelNumber();
      EnableButtons();
    }

    protected override void Subscribe()
    {
      _restart.onClick.AddListener(RestartThisLevel);
      _showRewardAd.onClick.AddListener(ShowRewardAd);

      _adsService.Rewarded.UserEarnedReward += ContinueGame;
    }

    protected override void Cleanup() =>
      _adsService.Rewarded.UserEarnedReward -= ContinueGame;

    private void ShowRewardAd()
    {
      if (_adsService.Rewarded.IsLoadedAd)
        _adsService.Rewarded.Show();
      else
      {
        _adsService.Rewarded.LoadAd();
        StartCoroutine(ShowMessageAdIsNotReady());
      }
    }

    private void ContinueGame(string nameReward, double amount)
    {
      _timer.AddSeconds((float) amount);
      Destroy(gameObject);
      Destroy(_background);
    }

    private void RestartThisLevel()
    {
      Destroy(gameObject);
      Destroy(_background);
      _levelTransfer.TransferToLevel(SceneManager.GetActiveScene().path);
    }

    private void RefreshCoins() =>
      _coins.text = _persistentProgress.PlayerProgress.PlayerCollections.Coins.ToString();

    private void RefreshLevelNumber() =>
      _numberLevel.text = (_staticData.ForLevelList().PathsScenes.IndexOf(SceneManager.GetActiveScene().path) + 1)
        .ToString();

    private void EnableButtons()
    {
      if (_adsService.Rewarded.IsLoadedAd)
      {
        EnableShowRewardAdButton();
        StartCoroutine(EnableRestartButtonAfterDuration());
      }
      else
        EnableRestartButton();
    }

    private IEnumerator EnableRestartButtonAfterDuration()
    {
      yield return new WaitForSecondsRealtime(_durationEnableRestartButton);
      EnableRestartButton();
    }

    private IEnumerator ShowMessageAdIsNotReady()
    {
      _adIsNotReady.SetActive(true);
      yield return new WaitForSecondsRealtime(_durationMessageAdIsNotReady);
      _adIsNotReady.SetActive(false);
    }

    private void EnableShowRewardAdButton() =>
      _showRewardAd.gameObject.SetActive(true);

    private void EnableRestartButton() =>
      _restart.gameObject.SetActive(true);
  }
}