﻿using TMPro;
using UnityEngine;

namespace UI.Windows
{
  public class BackgroundWindow : Window
  {
    [SerializeField] private TextMeshProUGUI _title = default;

    public void Construct(string title) =>
      _title.text = title;
  }
}