﻿using Services;
using Services.Factories.Game;
using Services.LevelTransfer;
using Services.PersistentProgressService;
using StaticData;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace UI.Windows
{
  public class EndLevelWindow : Window
  {
    [SerializeField] private TextMeshProUGUI _coins = default;
    [SerializeField] private TextMeshProUGUI _snakeLenght = default;
    [SerializeField] private Button _restart = default;
    [SerializeField] private Button _goToNextLevel = default;
    [SerializeField] private Button _goToNextLevelAfterAds = default;
    [SerializeField] private EndLevelWindowEffects _endLevelWindowEffects = default;

    private GameFactoryContainer _gameContainer;
    private LevelStaticData _levelStaticData;
    private ILevelTransferService _levelTransfer;
    private IPersistentProgressService _persistentProgress;
    private IAdsService _adsService;
    private GameObject _background;

    private bool _enableAd;

    private void Awake()
    {
      _goToNextLevel.onClick.AddListener(GoToNextLevel);
      _goToNextLevelAfterAds.onClick.AddListener(GoToNextLevelAfterAds);
      _restart.onClick.AddListener(RestartThisLevel);
    }
    
    public void Constructor(bool enableAd, GameFactoryContainer container,
      ILevelTransferService levelTransfer, IAdsService adsService, GameObject background,
      IPersistentProgressService persistentProgress, LevelStaticData levelStaticData)
    {
      _persistentProgress = persistentProgress;
      _gameContainer = container;
      _levelTransfer = levelTransfer;
      _adsService = adsService;
      _background = background;
      _enableAd = enableAd;
      _levelStaticData = levelStaticData;
    }

    protected override void Initialize()
    {
      RefreshCoins();
      RefreshSnakeLenght();
      EnableAdButton();
      _endLevelWindowEffects.Initialize(
        _levelStaticData.StarsProgress.SnakeLenghtOnLevel,
        _gameContainer.MovementTailChainsPlayer.ChainsCount - 1,
        _persistentProgress.PlayerProgress);
    }

    protected override void Subscribe()
    {
      _persistentProgress.PlayerProgress.PlayerCollections.Changed += RefreshCoins;
      _adsService.Interstitial.AdClosed += GoToNextLevel;
    }

    protected override void Cleanup()
    {
      _persistentProgress.PlayerProgress.PlayerCollections.Changed -= RefreshCoins;
      _adsService.Interstitial.AdClosed -= GoToNextLevel;
    }

    private void GoToNextLevel()
    {
      Destroy(gameObject);
      Destroy(_background);
      InversionLevelNumberParity(_persistentProgress);
      _levelTransfer.TransferToNextLevel(SceneManager.GetActiveScene().path);
    }

    private void GoToNextLevelAfterAds() =>
      _adsService.Interstitial.Show();

    private void RestartThisLevel()
    {
      Destroy(gameObject);
      Destroy(_background);
      _levelTransfer.TransferToLevel(SceneManager.GetActiveScene().path);
    }

    private void EnableAdButton()
    {
      if (_adsService.Interstitial.IsLoadedAd)
        _goToNextLevelAfterAds.gameObject.SetActive(_enableAd);
    }

    private void RefreshSnakeLenght() =>
      _snakeLenght.text = (_gameContainer.MovementTailChainsPlayer.ChainsCount - 1).ToString();

    private void RefreshCoins() =>
      _coins.text = _persistentProgress.PlayerProgress.PlayerCollections.Coins.ToString();

    private void InversionLevelNumberParity(IPersistentProgressService persistentProgress) =>
      persistentProgress.PlayerProgress.LevelNumber.Parity = !persistentProgress.PlayerProgress.LevelNumber.Parity;
  }
}