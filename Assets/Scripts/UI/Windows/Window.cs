﻿using UnityEngine;

namespace UI.Windows
{
  public class Window : MonoBehaviour
  {
    private void Start()
    {
      Initialize();
      Subscribe();
    }

    private void OnDestroy() =>
      Cleanup();

    protected virtual void Initialize() { }

    protected virtual void Subscribe() { }

    protected virtual void Cleanup() { }
  }
}