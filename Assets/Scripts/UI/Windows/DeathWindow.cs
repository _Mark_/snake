using Services;
using Services.LevelTransfer;
using Services.PersistentProgressService;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace UI.Windows
{
  public class DeathWindow : Window
  {
    [SerializeField] private Button _restart = default;
    [SerializeField] private Button _restartAfterAds = default;

    private ILevelTransferService _levelTransfer;
    private IAdsService _adsService;
    private GameObject _background;

    public void Constructor(ILevelTransferService levelTransfer, IAdsService adsService,
      GameObject background)
    {
      _levelTransfer = levelTransfer;
      _adsService = adsService;
      _background = background;
    }

    protected override void Initialize() =>
      EnableAdButton();

    protected override void Subscribe()
    {
      _restart.onClick.AddListener(RestartThisLevel);
      _restartAfterAds.onClick.AddListener(RestartThisLevelAfterAds);
      
      _adsService.Interstitial.AdClosed += RestartThisLevel;
    }

    protected override void Cleanup() =>
      _adsService.Interstitial.AdClosed -= RestartThisLevel;

    private void RestartThisLevel()
    {
      Destroy(gameObject);
      Destroy(_background);
      _levelTransfer.TransferToLevel(SceneManager.GetActiveScene().path);
    }

    private void RestartThisLevelAfterAds() =>
      _adsService.Interstitial.Show();

    private void EnableAdButton()
    {
      if (_adsService.Interstitial.IsLoadedAd)
        _restartAfterAds.gameObject.SetActive(true);
    }

    private void InversionLevelNumberParity(IPersistentProgressService persistentProgress) =>
      persistentProgress.PlayerProgress.LevelNumber.Parity = !persistentProgress.PlayerProgress.LevelNumber.Parity;
  }
}