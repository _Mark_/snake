using Services.LevelTransfer;
using Services.PersistentProgressService;
using Services.StaticData;
using Services.Windows;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace UI.Windows
{
  public class MenuWindow : Window
  {
    [SerializeField] private TextMeshProUGUI _coins = default;
    [SerializeField] private TextMeshProUGUI _numberLevel = default;
    [SerializeField] private Button _close = default;
    [SerializeField] private Button _restart = default;
    [SerializeField] private Button _classicMode = default;
    [SerializeField] private Button _returnToLastLevel = default;

    private ILevelTransferService _levelTransfer;
    private GameObject _background;
    private IWindowsService _windowsService;
    private IPersistentProgressService _persistentProgress;
    private IStaticDatasetService _staticData;

    public void Constructor(
      GameObject background, ILevelTransferService levelTransfer, IWindowsService windowsService,
      IPersistentProgressService persistentProgress, IStaticDatasetService staticData)
    {
      _levelTransfer = levelTransfer;
      _background = background;
      _windowsService = windowsService;
      _persistentProgress = persistentProgress;
      _staticData = staticData;
    }

    protected override void Initialize()
    {
      RefreshCoins();
      RefreshLevelNumber();
      FreezeTime();
    }

    protected override void Subscribe()
    {
      _close.onClick.AddListener(CloseWindow);
      _restart.onClick.AddListener(RestartThisLevel);
      _classicMode.onClick.AddListener(TransferToClassicModeLevel);
      _returnToLastLevel.onClick.AddListener(ReturnToLastLevel);
    }

    protected override void Cleanup() =>
      UnFreezeTime();

    private void CloseWindow()
    {
      Destroy(gameObject);
      Destroy(_background);
    }

    private void RestartThisLevel()
    {
      Destroy(gameObject);
      Destroy(_background);
      _levelTransfer.TransferToLevel(SceneManager.GetActiveScene().path);
    }

    private void TransferToClassicModeLevel()
    {
      Destroy(gameObject);
      Destroy(_background);
      _levelTransfer.TransferToLevel(Const.Scene.Name.ClassicMode);
    }

    private void ReturnToLastLevel()
    {
      Destroy(gameObject);
      Destroy(_background);
      string level = _persistentProgress.PlayerProgress.LastLevel.Path;
      if (string.IsNullOrEmpty(level))
        level = _staticData.ForLevelList().PathsScenes[0];
      
      _levelTransfer.TransferToLevel(level);
    }

    private void OpenLevelListWindow()
    {
      Destroy(gameObject);
      Destroy(_background);
      _windowsService.Open(WindowType.LevelListWindow);
    }

    private void RefreshCoins() =>
      _coins.text = _persistentProgress.PlayerProgress.PlayerCollections.Coins.ToString();

    private void RefreshLevelNumber()
    {
      _numberLevel.text = (_staticData.ForLevelList().PathsScenes.IndexOf(SceneManager.GetActiveScene().path) + 1)
        .ToString();
    }

    private void FreezeTime() =>
      Time.timeScale = 0;

    private void UnFreezeTime() =>
      Time.timeScale = 1;
  }
}