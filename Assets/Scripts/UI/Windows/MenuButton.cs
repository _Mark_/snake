using Services.Windows;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Windows
{
  public class MenuButton : MonoBehaviour
  {
    [SerializeField] private Button _button = default;

    private IWindowsService _windowsService;

    public void Constructor(IWindowsService windowsService) =>
      _windowsService = windowsService;

    private void OnEnable() =>
      _button.onClick.AddListener(OpenMenu);

    private void OnDisable() =>
      _button.onClick.RemoveListener(OpenMenu);

    private void OpenMenu() =>
      _windowsService.Open(WindowType.Menu);
  }
}