﻿using System.Collections;
using Data;
using TMPro;
using UnityEngine;

namespace UI.Windows
{
  public class EndLevelWindowEffects : MonoBehaviour
  {
    private const int _countSections = Const.GamePlay.QuantityStars + 1;
    
    [SerializeField] private float _timeForOpenStar = 0.5f;
    [SerializeField] private float _timeForAccrualOfCoins = 0.01f;
    [SerializeField] private TextMeshProUGUI _snakeLenght = default;
    [SerializeField] private GameObject[] _starsBackground = default;
    [SerializeField] private GameObject[] _stars = default;
    private PlayerProgress _playerProgress;

    public void Initialize(int lengthSnakeOnLevel, int lenghtSnake, PlayerProgress playerProgress)
    {
      _playerProgress = playerProgress;
      StartCoroutine(FillingStars(CountingStars(lengthSnakeOnLevel, lenghtSnake)));
      StartCoroutine(AccrualOfCoins(lenghtSnake));
    }

    private IEnumerator FillingStars(int quantityStars)
    {
      for (int i = 0; i < quantityStars; i++)
      {
        yield return new WaitForSeconds(_timeForOpenStar);
        _starsBackground[i].SetActive(false);
        _stars[i].SetActive(true);
      }
    }

    private IEnumerator AccrualOfCoins(int lenghtSnake)
    {
      while (lenghtSnake > 0)
      {
        yield return new WaitForSeconds(_timeForAccrualOfCoins);

        DecreaseSnakeLenghtText();
        AccrualOfMoney();

        lenghtSnake--;
      }
    }

    private int CountingStars(int lengthSnakeOnLevel, int lenghtSnake)
    {
      int quantityStars = 0;
      float diapason = (float) lengthSnakeOnLevel / _countSections;
      float diapasonSum = diapason;

      for (int i = 0; i < Const.GamePlay.QuantityStars; i++)
      {
        if (diapasonSum >= lenghtSnake)
          break;
        diapasonSum += diapason;
        quantityStars++;
      }

      return quantityStars;
    }

    private void AccrualOfMoney() =>
      _playerProgress.PlayerCollections.Collect(1);

    private void DecreaseSnakeLenghtText()
    {
      int snakeLenght = int.Parse(_snakeLenght.text);
      _snakeLenght.text = (--snakeLenght).ToString();
    }
  }
}