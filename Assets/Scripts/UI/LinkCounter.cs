﻿using GameMechanics.Snake.Movement;
using TMPro;
using UnityEngine;

namespace UI
{
  public class LinkCounter : MonoBehaviour
  {
    public TextMeshProUGUI CountChainTail;
    public MovementTailChains MovementTailChains;

    public void Constructor(MovementTailChains movementTailChains)
    {
      MovementTailChains = movementTailChains;
      MovementTailChains.ChainChanged += UpdateCountChainTail;
      UpdateCountChainTail();
    }

    private void UpdateCountChainTail() =>
      CountChainTail.text = (MovementTailChains.ChainsCount - 1).ToString();

    private void OnDestroy() =>
      MovementTailChains.ChainChanged -= UpdateCountChainTail;
  }
}