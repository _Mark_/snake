using GameMechanics.TimerForLevel;
using TMPro;
using UnityEngine;

namespace UI
{
  public class DisplayTimer : MonoBehaviour
  {
    [SerializeField] private TextMeshProUGUI _text = default;
    [SerializeField] private TimerOfLevel _timer = default;

    private void OnEnable() =>
      _timer.Changed += UpdateText;

    private void OnDisable() =>
      _timer.Changed -= UpdateText;

    private void UpdateText() =>
      _text.text = _timer.RemainingTime.ToString("##");
  }
}