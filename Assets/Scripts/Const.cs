﻿public static class Const
{
  public static class ResourcesPaths
  {
    public const string JoystickPathUI = "Prefabs/UI/JoystickCanvas";
    public const string SnakeTailLenghtUI = "Prefabs/UI/SnakeScreenData";
    public const string SnakeHeadPath = "Prefabs/SnakeHead";
    public const string EnemySnakeHead = "Prefabs/EnemySnakeHead";
    public const string LevelTransferTrigger = "Prefabs/LevelTransferTrigger";
    public const string LookAtPositionController = "Prefabs/LookAtPositionController";
    public const string ChainTailSnake = "Prefabs/ChainTailSnake";
    public const string EnemySnakeTailChain = "Prefabs/EnemyTailChain";
    public const string Apple = "Prefabs/Apple";
    public const string BadApple = "Prefabs/BabApple";
    public const string Skull = "Prefabs/Skull";
    public const string Scope = "Prefabs/Scope";
    public const string SpawnerOfObjectsForSnake = "Prefabs/SpawnerOfObjectsForSnake";

    public static class UI
    {
      public const string Root = "Prefabs/UI/Windows/UIRoot";
      public const string BackgroundWindow = "Prefabs/UI/Windows/Window";
      public const string EndLevelWindow = "Prefabs/UI/Windows/EndLevelWindow";
      public const string DeathWindow = "Prefabs/UI/Windows/DeathWindow";
      public const string MenuWindow = "Prefabs/UI/Windows/MenuWindow";
      public const string LevelListWindow = "Prefabs/UI/Windows/LevelListWindow";
      public const string TimeEndWindow = "Prefabs/UI/Windows/TimeEndWindow";
    }
  }

  public static class Tag
  {
    public static string InitialPoint = "InitialPoint";
    public static string DevelopMarker = "DevelopMarker";
    public static string Player = "Player";
  }

  public static class Scene
  {
    public static class Name
    {
      public const string ClassicMode = "ClassicMode";
    }
    public static class Hierarchy
    {
      public const string UIRoot = "UIRoot"; 
    }
  }

  public static class GamePlay
  {
    public const int QuantityStars = 3;
  }
}